-- MySQL dump 10.14  Distrib 5.5.60-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: ng_controller
-- ------------------------------------------------------
-- Server version	5.5.60-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agent_api_keys`
--

DROP TABLE IF EXISTS `agent_api_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_api_keys` (
  `id` varchar(12) COLLATE utf8_bin NOT NULL,
  `alias` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `key_hash` varchar(75) COLLATE utf8_bin NOT NULL,
  `ip_address_pool` longtext COLLATE utf8_bin,
  `registration_timestamp` datetime DEFAULT NULL,
  `status` varchar(15) COLLATE utf8_bin DEFAULT 'Active',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alembic_version`
--

DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alembic_version` (
  `version_num` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `config_value` longtext COLLATE utf8_bin,
  `content_hash` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_hash` (`content_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `host_groups`
--

DROP TABLE IF EXISTS `host_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `host_groups` (
  `id` varchar(12) COLLATE utf8_bin NOT NULL,
  `alias` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `registered_host_count` int(11) DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  `registration_timestamp` datetime DEFAULT NULL,
  `status` varchar(15) COLLATE utf8_bin DEFAULT 'Active',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `host_groups`
--

LOCK TABLES `host_groups` WRITE;
/*!40000 ALTER TABLE `host_groups` DISABLE KEYS */;
INSERT INTO `host_groups` VALUES ('2c10a26a202c','Default',2,1,'2019-07-10 02:58:19','Active',0);
/*!40000 ALTER TABLE `host_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hosts`
--

DROP TABLE IF EXISTS `hosts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(75) COLLATE utf8_bin NOT NULL,
  `host_uuid` varchar(33) COLLATE utf8_bin NOT NULL,
  `primary_address` varchar(30) COLLATE utf8_bin NOT NULL,
  `hypervisor` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `build_config` longtext COLLATE utf8_bin,
  `build_config_hash` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `tags` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `host_group` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `agent_version` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `os_info` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `host_boot_time` datetime DEFAULT NULL,
  `nginx_boot_time` datetime DEFAULT NULL,
  `running` tinyint(1) DEFAULT NULL,
  `nginx_package_version` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `stub_status_enabled` tinyint(1) DEFAULT NULL,
  `built_from_source` tinyint(1) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `registration_timestamp` datetime DEFAULT NULL,
  `status` varchar(15) COLLATE utf8_bin DEFAULT 'Active',
  `deleted` tinyint(1) DEFAULT '0',
  `latest_config` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  UNIQUE KEY `host_uuid` (`host_uuid`),
  UNIQUE KEY `primary_address` (`primary_address`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nginx_configuration`
--

DROP TABLE IF EXISTS `nginx_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nginx_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host_group` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `config_file` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `config_file_size` int(11) DEFAULT NULL,
  `config_file_modify_time` int(11) DEFAULT NULL,
  `config_file_lines` int(11) DEFAULT NULL,
  `config_status` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `config_errors` longtext COLLATE utf8_bin,
  `config_content` longtext COLLATE utf8_bin,
  `config_content_hash` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `config_update_required` tinyint(1) DEFAULT '0',
  `config_update_failed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `who_is_see` longtext COLLATE utf8_bin,
  `icon` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(15) COLLATE utf8_bin DEFAULT 'Active',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `location` (`location`),
  UNIQUE KEY `icon` (`icon`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Dashboard','/main/dashboard','1;2','home','Active',0),(2,'Clusters','/clusters/','1;2','cpu','Active',0),(3,'System Events','/main/system-events','2','alert-triangle','Active',0);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_events`
--

DROP TABLE IF EXISTS `system_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(75) COLLATE utf8_bin DEFAULT 'warning',
  `event` longtext COLLATE utf8_bin,
  `registration_timestamp` datetime DEFAULT NULL,
  `event_seen` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(75) COLLATE utf8_bin NOT NULL,
  `permissions` longtext COLLATE utf8_bin NOT NULL,
  `content_hash` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `registration_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_hash` (`content_hash`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'User','modify_user_password_func,list_agent_api_key_func,list_host_group_func,list_user_func,list_user_role_func,list_host_info_func,list_hosts_func,list_system_events_func','202e8fc64b3da70cbdb513c4c9b10c87fc1814f21b89b40a602c74ead1a181ae','2019-07-14 12:56:10'),(2,'Admin','all','5ef5ef0364b6939c4ca61f34b393f7b368d1be8619647aaf83d5b395919ab629','2019-07-14 12:56:12');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` varchar(12) COLLATE utf8_bin NOT NULL,
  `email` varchar(200) COLLATE utf8_bin NOT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(75) COLLATE utf8_bin NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `registration_timestamp` datetime DEFAULT NULL,
  `role` tinyint(1) DEFAULT NULL,
  `status` varchar(15) COLLATE utf8_bin DEFAULT 'Active',
  `deleted` tinyint(1) DEFAULT '0',
  `protected` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('as12er23ty45','root@local','root','9e63ff7c54ee9343dcd9f62fe7bd79fe031b42f5f56d9d674e630194b2ab4b47','Root','Root','2019-07-06 18:25:20',2,'Active',0,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-29 20:06:20
