USE ng_controller;

DELIMITER $$
CREATE FUNCTION get_role_name(role_id TINYINT(1)) RETURNS VARCHAR(75)
	DETERMINISTIC
BEGIN
	DECLARE rn VARCHAR(75);
	SELECT role_name INTO rn
	FROM user_roles
	WHERE id = role_id;
	RETURN rn;
END$$