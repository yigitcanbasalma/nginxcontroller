#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import and_
from datetime import datetime, timedelta
from app import db, logger, remote_agent_configurations
from app.extensions.general_ext import get_host_group_alias, new_system_event
from app.modules.agent.models import Hosts
from app.modules.agent.models import NginxConfiguration


def cluster_configuration_sync_controller():
    try:
        sync_wait_clusters_object = NginxConfiguration.query.filter(
            NginxConfiguration.config_update_required == 1
        ).all()
        for cluster in sync_wait_clusters_object:
            sync_wait_hosts = Hosts.query.with_entities(
                Hosts.id
            ).filter(
                and_(
                    Hosts.host_group == cluster.host_group,
                    Hosts.latest_config == 0
                )
            ).all()
            if not sync_wait_hosts:
                cluster.config_update_required = 0
                db.session.commit()
                log = "Cluster config updated. Cluster: '{0}'.".format(get_host_group_alias(cluster.host_group))
                logger.write_log(
                    event_type="lb-cluster-configuration-sync-controller-batch",
                    event_severity="INFO",
                    event_username="system",
                    event_ip="127.0.0.1",
                    event=log
                )
    except KeyboardInterrupt as e:
        logger.write_log(
            event_type="cluster-configuration-sync-controller-batch",
            event_severity="ERROR",
            event_username="system",
            event_ip="127.0.0.1",
            event=e
        )


def amplify_agent_status_controller():
    try:
        all_hosts_object = Hosts.query.filter(
            Hosts.deleted == 0
        ).all()
        max_talk_interval = remote_agent_configurations["config"]["cloud"]["talk_interval"] + 60  # Add extra 60 seconds
        for host in all_hosts_object:
            if host.last_update < (datetime.now() - timedelta(seconds=max_talk_interval)):
                if host.agent_status:
                    host.agent_status = False
                    new_system_event(
                        event="Amplify agent not running on '{0}'.".format(host.hostname),
                        event_type="error"
                    )
            else:
                if not host.agent_status:
                    host.agent_status = True
                    new_system_event(
                        event="Amplify agent running now on '{0}'.".format(host.hostname),
                        event_type="success"
                    )
    except KeyboardInterrupt as e:
        logger.write_log(
            event_type="amplify-agent-status-controller-batch",
            event_severity="ERROR",
            event_username="system",
            event_ip="127.0.0.1",
            event=e
        )
    finally:
        db.session.commit()
