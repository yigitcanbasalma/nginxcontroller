#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import datetime
from rq_scheduler import Scheduler

# Scheduled functions
from app.scheduled_jobs.clusters_batch_jobs import cluster_configuration_sync_controller, amplify_agent_status_controller


# Schedule all jobs
def schedule_all(rq_queue, connection):
    scheduler = Scheduler(
        queue=rq_queue,
        connection=connection
    )
    scheduler.schedule(
        scheduled_time=datetime.utcnow(),               # Time for first execution, in UTC timezone
        func=cluster_configuration_sync_controller,     # Function to be queued
        interval=30,                                    # Time before the function is called again, in seconds
        repeat=None                                     # Repeat this number of times (None means repeat forever)
    )
    scheduler.schedule(
        scheduled_time=datetime.utcnow(),       # Time for first execution, in UTC timezone
        func=amplify_agent_status_controller,   # Function to be queued
        interval=30,                            # Time before the function is called again, in seconds
        repeat=None                             # Repeat this number of times (None means repeat forever)
    )
