#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import and_
from datetime import datetime
from os.path import join as dir_join
from crossplane import build
from app import db, logger, remote_agent_configurations, nginx_base_location, nginx_config_location
from app.extensions.response_ext import response_create
from app.extensions.queue_ext import queue_to_job
from app.extensions.db_ext import calculate_hash
from app.extensions.general_ext import get_host_group_alias, new_system_event, nginx_config_parser
from app.modules.admin.models import HostGroups
from app.modules.agent.models import Hosts, NginxConfiguration


def register_host(post_form, user_ip, api_key):
    try:
        new_remote_agent_configurations = remote_agent_configurations.copy()
        if len(post_form) > 0:
            hostname = post_form["hostname"]
            host_uuid = post_form["uuid"]
            is_exists = Hosts.query.filter(
                and_(
                    Hosts.hostname == hostname,
                    Hosts.host_uuid == host_uuid,
                    Hosts.primary_address == user_ip
                )
            ).first()
            if not is_exists:
                default_host_group = HostGroups.query.filter(
                    HostGroups.is_default == 1
                ).first()
                new_host = Hosts(
                    hostname=hostname,
                    host_uuid=host_uuid,
                    primary_address=user_ip,
                    host_group=default_host_group.id,
                    last_update=datetime.now()
                )
                default_host_group.registered_host_count += 1
                db.session.add(new_host)
                log = "New host created. Hostname: '{0}', Host UUID: '{1}'.".format(hostname, host_uuid)
                logger.write_log(
                    event_type="agent-host-registration",
                    event_severity="INFO",
                    event_username="system",
                    event_ip=user_ip,
                    event=log
                )
            elif is_exists.deleted == 1:
                host_group_object = HostGroups.query.filter(
                    HostGroups.id == is_exists.host_group
                ).first()
                host_group_object.registered_host_count += 1
                is_exists.deleted = 0
            elif is_exists.latest_config == 0:
                is_exists.latest_config = 1
                config_object = dict()
                all_cluster_configs_object = NginxConfiguration.query.with_entities(
                    NginxConfiguration.config_file,
                    NginxConfiguration.config_content
                ).filter(
                    and_(
                        NginxConfiguration.host_group == is_exists.host_group,
                        NginxConfiguration.config_update_required == 1
                    )
                ).all()
                for config in all_cluster_configs_object:
                    config_object[config.config_file] = nginx_config_parser(config.config_content)
                new_remote_agent_configurations["nginx_config"] = config_object
            else:
                all_cluster_configs_object = NginxConfiguration.query.with_entities(
                    NginxConfiguration.config_file
                ).filter(
                    NginxConfiguration.host_group == is_exists.host_group
                ).all()
                new_remote_agent_configurations["nginx_config_files"] = [i.config_file for i in all_cluster_configs_object]
                is_exists.last_update = datetime.now()
        db.session.commit()
        return response_create(
            data=new_remote_agent_configurations,
            headers={"X-Key-Prefix": api_key[:3]}
        )
    except KeyboardInterrupt as e:
        logger.write_log(
            event_type="agent-host-registration",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )


def queue_update_registered_host(post_form, user_ip, api_key):
    try:
        queue_to_job(
            func_name=update_registered_host,
            args=(post_form, user_ip)
        )
        return response_create(
            data={},
            headers={"X-Key-Prefix": api_key[:3]}
        )
    except KeyboardInterrupt as e:
        logger.write_log(
            event_type="agent-host-info-update-queue",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )


def update_registered_host(post_form, user_ip):
    try:
        meta = post_form["meta"][0]
        metrics = post_form["metrics"]
        configs = post_form["configs"][0] if len(post_form["configs"]) > 0 else post_form["configs"]
        events = post_form["events"]
        hostname = meta["hostname"]
        host_uuid = meta["uuid"]
        host_object = Hosts.query.filter(
            and_(
                Hosts.hostname == hostname,
                Hosts.host_uuid == host_uuid,
                Hosts.status == "Active",
                Hosts.deleted == 0
            )
        ).first()
        if host_object:
            # Meta Configuration Area
            # Meta information update prep
            meta_info_map = {
                "hypervisor": meta["processor"]["hypervisor"],
                "build_config": ";".join([str(k) + ": " + str(v) for k, v in meta["children"][0]["configure"].items()]) if "children" in meta else None,
                "tags": ";".join([str(i["value"]) for i in meta["tags"]]),
                "agent_version": meta["agent"],
                "os_info": meta["release"]["name"] + " " + meta["release"]["version"],
                "host_boot_time": datetime.fromtimestamp(meta["boot"] / 1000),
                "nginx_boot_time": datetime.fromtimestamp(meta["children"][0]["start_time"] / 1000) if "children" in meta else None,
                "running": meta["children"][0]["running"] if "children" in meta else False,
                "nginx_package_version": meta["children"][0]["packages"]["nginx"] if "children" in meta else None,
                "stub_status_enabled": meta["children"][0]["stub_status_enabled"] if "children" in meta else None,
                "built_from_source": meta["children"][0]["built_from_source"] if "children" in meta else None
            }
            meta_info_map["build_config_hash"] = calculate_hash(
                target=meta_info_map["build_config"],
                method="md5"
            ) if "children" in meta else None
            # Check there is any change
            for k, v in meta_info_map.items():
                if getattr(host_object, k) != v:
                    setattr(host_object, k, v)

            # Config Configuration Area
            if len(configs) > 0:
                # If host in "Default" named host group, do nothing
                if host_object.host_group not in ["2c10a26a202c"]:
                    for config in configs["children"][0]["config"]["data"]["tree"]["config"]:
                        built_config = build(config["parsed"])
                        built_config_hash = calculate_hash(built_config)
                        config_obj = NginxConfiguration.query.filter(
                            and_(
                                NginxConfiguration.host_group == host_object.host_group,
                                NginxConfiguration.config_file == config["file"]
                            )
                        ).first()
                        if config_obj:
                            if config_obj.config_content_hash != built_config_hash:
                                if config_obj.config_update_required == 0:
                                    config_obj.config_update_required = 1
                                    host_object.latest_config = 0
                                log = "Illegal changes found for '{0}' named host group.This will be fixed. File: '{1}', Host: '{2}'.".format(get_host_group_alias(host_object.host_group), config["file"], host_object.hostname)
                                logger.write_log(
                                    event_type="agent-host-info-update",
                                    event_severity="ERROR",
                                    event_username="system",
                                    event_ip=user_ip,
                                    event=log
                                )
                                new_system_event(
                                    event_type="error",
                                    event=log
                                )
                        else:
                            # New configuration file found
                            if config["file"].startswith(nginx_config_location) or config["file"] in [dir_join(nginx_base_location, "nginx.conf")]:
                                new_config = NginxConfiguration(
                                    host_group=host_object.host_group,
                                    config_file=config["file"],
                                    config_file_size=configs["children"][0]["config"]["data"]["files"][config["file"]]["size"],
                                    config_file_modify_time=configs["children"][0]["config"]["data"]["files"][config["file"]]["mtime"],
                                    config_file_lines=configs["children"][0]["config"]["data"]["files"][config["file"]]["lines"],
                                    config_status=config["status"],
                                    config_errors=";".join([
                                        "Found an error at line '{0}'. Error: '{1}'.".format(
                                            i["line"], i["error"]
                                        ) for i in config["errors"]
                                    ]),
                                    config_content=built_config,
                                    config_content_hash=built_config_hash
                                )
                                db.session.add(new_config)
                                log = "New config file added to '{0}' named host group. File: '{1}'.".format(get_host_group_alias(host_object.host_group), config["file"])
                                logger.write_log(
                                    event_type="agent-host-info-update",
                                    event_severity="INFO",
                                    event_username="system",
                                    event_ip=user_ip,
                                    event=log
                                )
    except KeyboardInterrupt as e:
        logger.write_log(
            event_type="agent-host-info-update",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
    finally:
        db.session.commit()
