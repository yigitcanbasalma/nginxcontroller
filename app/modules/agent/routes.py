#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Blueprint, request
from app.extensions.wrappers import before_request_processing
from app.extensions.response_ext import compressed_response_extract, response_create
from app.modules.agent.controllers import register_host, queue_update_registered_host


agent_router = Blueprint("agent_router", __name__, url_prefix="/agent")


@agent_router.route("/ping/", methods=["GET"])
@before_request_processing(api_restricted=False)
def agent_dummy_check():
    return response_create(
        data="pong"
    )


@agent_router.route("/<agent_api_key>/agent/", methods=["POST"])
@before_request_processing(api_restricted=True)
def agent_health_check(agent_api_key):
    return register_host(
        post_form=compressed_response_extract(response=request.data),
        user_ip=request.headers.get("X-Forwarded-For"),
        api_key=agent_api_key
    )


@agent_router.route("/<agent_api_key>/update/", methods=["POST"])
@before_request_processing(api_restricted=True)
def update_agent_data(agent_api_key):
    return queue_update_registered_host(
        post_form=compressed_response_extract(response=request.data),
        user_ip=request.headers.get("X-Forwarded-For"),
        api_key=agent_api_key
    )
