#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import db, Base
from sqlalchemy.dialects.mysql import TINYINT, LONGTEXT


class Hosts(Base):
    __tablename__ = "hosts"

    hostname = db.Column(db.String(75), nullable=False, unique=True)
    host_uuid = db.Column(db.String(33), nullable=False, unique=True)
    primary_address = db.Column(db.String(30), nullable=False, unique=True)
    hypervisor = db.Column(db.String(75))
    build_config = db.Column(LONGTEXT)
    build_config_hash = db.Column(db.String(75))
    tags = db.Column(db.String(300))
    host_group = db.Column(db.String(12))
    agent_version = db.Column(db.String(75))
    os_info = db.Column(db.String(150))
    host_boot_time = db.Column(db.DateTime)
    nginx_boot_time = db.Column(db.DateTime)
    running = db.Column(TINYINT(1))
    nginx_package_version = db.Column(db.String(75))
    stub_status_enabled = db.Column(TINYINT(1))
    built_from_source = db.Column(TINYINT(1))
    last_update = db.Column(db.DateTime)
    registration_timestamp = db.Column(db.DateTime, default=db.func.current_timestamp())
    status = db.Column(db.String(15), server_default="Active")
    deleted = db.Column(TINYINT(1), server_default="0")
    latest_config = db.Column(TINYINT(1), server_default="1")
    agent_status = db.Column(TINYINT(1), server_default="1")

    def __init__(self, **kwargs):
        super(Hosts, self).__init__(**kwargs)

    def __repr__(self):
        return '<Host %r>' % self.id


class NginxConfiguration(Base):
    __tablename__ = "nginx_configuration"

    host_group = db.Column(db.String(12))
    config_file = db.Column(db.String(255))
    config_file_size = db.Column(db.Integer)
    config_file_modify_time = db.Column(db.Integer)
    config_file_lines = db.Column(db.Integer)
    config_status = db.Column(db.String(15))
    config_errors = db.Column(LONGTEXT)
    config_content = db.Column(LONGTEXT)
    config_content_hash = db.Column(db.String(75))
    config_update_required = db.Column(TINYINT(1), server_default="0")
    config_update_failed = db.Column(TINYINT(1), server_default="0")

    def __init__(self, **kwargs):
        super(NginxConfiguration, self).__init__(**kwargs)

    def __repr__(self):
        return '<NginxConfiguration %r>' % self.id
