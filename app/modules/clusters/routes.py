#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Blueprint, render_template, request
from app.extensions.wrappers import before_request_processing, login_required
from app.extensions.db_ext import datetime_from_timestamp
from app.extensions.general_ext import get_host_group_id
from app.modules.clusters.controllers import list_hosts_information, modify_host, delete_host, list_host_information, list_host_group_configuration_information, modify_clusters_configuration

clusters_router = Blueprint("clusters_router", __name__, url_prefix="/clusters")


@clusters_router.route("/", methods=["GET"])
@login_required
@before_request_processing(restricted=False)
def clusters_dashboard_page():
    return render_template(
        "clusters/dashboard.html"
    )


@clusters_router.route("/cluster/lb/configuration/<cluster_name>", methods=["GET"])
@login_required
@before_request_processing(restricted=False)
def clusters_configuration_page(cluster_name):
    return render_template(
        "clusters/lb_configuration.html",
        cluster_namne=cluster_name,
        host_group_id=get_host_group_id(cluster_name),
        host_group_config_info=list_host_group_configuration_information(
            host_group_name=cluster_name,
            user_ip=request.headers.get("X-Forwarded-For")
        ),
        timestamp_converter=datetime_from_timestamp
    )


@clusters_router.route("/hosts/list", methods=["GET"])
@login_required
@before_request_processing(restricted=True)
def list_hosts_func():
    return list_hosts_information(
        user_ip=request.headers.get("X-Forwarded-For")
    )


@clusters_router.route("/hosts/modify", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def modify_host_func():
    return modify_host(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@clusters_router.route("/hosts/delete/<host_id>", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def delete_host_func(host_id):
    return delete_host(
        host_id=host_id,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@clusters_router.route("/hosts/host-info/<host_id>", methods=["GET"])
@login_required
@before_request_processing(restricted=True)
def list_host_info_func(host_id):
    return list_host_information(
        host_id=host_id,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@clusters_router.route("/cluster/lb/configuration/modify", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def modify_clusters_configuration_func():
    return modify_clusters_configuration(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )
