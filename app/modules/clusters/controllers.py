#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import desc, and_
from crossplane import build
from flask import session
from app import db, logger
from app.extensions.response_ext import response_create
from app.extensions.general_ext import get_host_group_alias, get_host_group_id, sqlalchemy_as_dict, nginx_config_parser
from app.extensions.security_ext import escape_chars, BoolStringArgument
from app.extensions.db_ext import calculate_hash, get_unix_timestamp
from app.modules.admin.models import HostGroups
from app.modules.agent.models import Hosts, NginxConfiguration


def list_hosts_information(user_ip):
    try:
        hosts_with_host_group = dict()
        hosts_info = Hosts.query.with_entities(
            Hosts.id,
            Hosts.hostname,
            Hosts.primary_address,
            Hosts.hypervisor,
            Hosts.running,
            Hosts.os_info,
            Hosts.nginx_package_version,
            Hosts.host_group,
            Hosts.status,
            Hosts.tags
        ).filter(
            Hosts.deleted == 0
        ).order_by(
            desc(
                Hosts.registration_timestamp
            )
        ).all()
        for host in hosts_info:
            host_object = list(host)
            host_group = get_host_group_alias(host_group_id=host.host_group)
            host_object.append(host_group)
            if host_group in hosts_with_host_group.keys():
                hosts_with_host_group[host_group].append(host_object)
            else:
                hosts_with_host_group[host_group] = [
                    host_object
                ]
        return response_create({"status": "success", "hosts_info": hosts_with_host_group})
    except Exception as e:
        logger.write_log(
            event_type="hosts-list",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def modify_host(post_form, user_ip):
    try:
        status = BoolStringArgument(target=post_form["status"], data_type="Status").play()
        host_group_id = escape_chars(post_form["host_group"])
        host_id = escape_chars(post_form["host_id"])
        host_object = Hosts.query.filter(
            and_(
                Hosts.id == host_id,
                Hosts.deleted == 0
            )
        ).first()
        if host_object:
            is_changed = False
            changes = dict()
            if host_object.status != status:
                is_changed = True
                changes["status"] = {
                    "old": host_object.status,
                    "new": status
                }
                host_object.status = status
            if host_object.host_group != host_group_id:
                is_changed = True
                changes["host_group"] = {
                    "old": get_host_group_alias(host_group_id=host_object.host_group),
                    "new": get_host_group_alias(host_group_id=host_group_id)
                }
                # TODO: Create database function for this
                new_host_group_object = HostGroups.query.filter(
                    HostGroups.id == host_group_id,
                ).first()
                old_host_group_object = HostGroups.query.filter(
                    HostGroups.id == host_object.host_group,
                ).first()
                new_host_group_object.registered_host_count += 1
                old_host_group_object.registered_host_count -= 1
                host_object.host_group = host_group_id
            if is_changed:
                db.session.commit()
                log = "Host modified. Changes: '{0}', Username: '{1}', Hostname: '{2}'.".format("; ".join([k + ": " + str(v) for k, v in changes.items()]), session.get("username"), host_object.hostname)
                logger.write_log(
                    event_type="host-modify",
                    event_severity="INFO",
                    event_username="system",
                    event_ip=user_ip,
                    event=log
                )
                return response_create({
                    "status": "success",
                    "message": "Host modified.",
                })
        return response_create({
            "status": "warning",
            "message": "Nothings changed."
        })
    except Exception as e:
        logger.write_log(
            event_type="hosts-modify",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def delete_host(host_id, user_ip):
    try:
        host_object = Hosts.query.filter(
            and_(
                Hosts.id == host_id,
                Hosts.deleted == 0
            )
        ).first()
        if host_object:
            host_group_object = HostGroups.query.filter(
                HostGroups.id == host_object.host_group
            ).first()
            host_group_object.registered_host_count -= 1
            host_object.deleted = 1
            db.session.commit()
            log = "Host deleted. Username: '{0}', Hostname: '{1}'.".format(session.get("username"), host_object.hostname)
            logger.write_log(
                event_type="host-delete",
                event_severity="INFO",
                event_username="system",
                event_ip=user_ip,
                event=log
            )
            return response_create({
                "status": "success",
                "message": "Removing the system will remove all of the objects below. Do not forget to stop the Amplify Agent on the host, otherwise the system will reappear in the user interface"
            })
    except Exception as e:
        logger.write_log(
            event_type="host-delete",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def list_host_information(host_id, user_ip):
    try:
        host_info = Hosts.query.filter(
            and_(
                Hosts.deleted == 0,
                Hosts.id == host_id
            )
        ).first()
        host_info_dict = sqlalchemy_as_dict(result_proxy=host_info)
        host_info_dict["host_group_name"] = get_host_group_alias(host_group_id=host_info_dict["host_group"])
        return response_create({"status": "success", "host_info": host_info_dict})
    except Exception as e:
        logger.write_log(
            event_type="hosts-list",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def list_host_group_configuration_information(host_group_name, user_ip):
    try:
        host_group_configuration_info = NginxConfiguration.query.filter(
            NginxConfiguration.host_group == get_host_group_id(host_group_name)
        ).all()
        return host_group_configuration_info
    except Exception as e:
        logger.write_log(
            event_type="host-group-configuration-list",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )


def modify_clusters_configuration(post_form, user_ip):
    try:
        configuration = escape_chars(post_form["configuration"])
        host_group_id = escape_chars(post_form["host_group_id"])
        config_id = escape_chars(post_form["config_id"])
        configuration_object = NginxConfiguration.query.filter(
            and_(
                NginxConfiguration.host_group == host_group_id,
                NginxConfiguration.id == config_id
            )
        ).first()
        if configuration_object:
            new_config_object = nginx_config_parser(configuration)
            new_configuration = build(new_config_object["config"][0]["parsed"])
            config_content_hash = calculate_hash(new_configuration)
            if configuration_object.config_content_hash != config_content_hash:
                all_hosts_object = Hosts.query.filter(
                    and_(
                        Hosts.host_group == configuration_object.host_group,
                        Hosts.deleted == 0
                    )
                ).all()
                for host in all_hosts_object:
                    host.latest_config = 0
                # TODO: Take a backup for old configurations
                configuration_object.config_content = new_configuration
                configuration_object.config_content_hash = config_content_hash
                configuration_object.config_file_modify_time = get_unix_timestamp()
                configuration_object.config_update_required = 1
                db.session.commit()
                log = "Configuration modified by '{0}'. Host Group: '{1}'.".format(session.get("username"), get_host_group_alias(configuration_object.host_group))
                logger.write_log(
                    event_type="cluster-lb-configuration-modify",
                    event_severity="INFO",
                    event_username="system",
                    event_ip=user_ip,
                    event=log
                )
                return response_create({
                    "status": "success",
                    "message": "Configuration changed."
                })
        return response_create({
            "status": "warning",
            "message": "Nothings changed."
        })
    except Exception as e:
        logger.write_log(
            event_type="cluster-lb-configuration-modify",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})
