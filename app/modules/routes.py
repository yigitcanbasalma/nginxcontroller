#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Blueprint, render_template, request
from app.extensions.wrappers import before_request_processing

router = Blueprint("router", __name__)


@router.route("/", methods=["GET"])
@before_request_processing(restricted=False, api_restricted=False)
def index_page():
    return render_template(
        "signin.html",
        account_status_changed_sign=request.args.get("account_status_changed_sign", False)
    )
