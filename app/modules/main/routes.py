#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Blueprint, render_template, request
from app.extensions.wrappers import before_request_processing, login_required
from app.modules.main.controllers import list_system_events, modify_system_events, modify_user_password

main_router = Blueprint("main_router", __name__, url_prefix="/main")


@main_router.route("/dashboard", methods=["GET"])
@login_required
@before_request_processing(restricted=False)
def dashboard_page():
    return render_template(
        "main/dashboard.html"
    )


@main_router.route("/profile", methods=["GET"])
@login_required
@before_request_processing(restricted=False)
def profile_page():
    return render_template(
        "main/profile.html"
    )


@main_router.route("/profile/password/modify", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def modify_user_password_func():
    return modify_user_password(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@main_router.route("/system-events", methods=["GET"])
@login_required
@before_request_processing(restricted=False)
def system_events_page():
    return render_template(
        "main/system_events.html"
    )


@main_router.route("/system-events/list", methods=["GET"])
@login_required
@before_request_processing(restricted=True)
def list_system_events_func():
    return list_system_events(
        fields=request.args.get("fields", None),
        count=request.args.get("count", False),
        user_ip=request.headers.get("X-Forwarded-For")
    )


@main_router.route("/system-events/modify", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def modify_system_events_func():
    return modify_system_events(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )
