#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import desc, and_
from flask import session
from app import logger, db
from app.extensions.response_ext import response_create
from app.extensions.security_ext import escape_chars, PasswordArgument
from app.extensions.db_ext import calculate_hash
from app.modules.auth.models import Users
from app.modules.main.models import SystemEvents


def list_system_events(fields, count, user_ip):
    try:
        available_fields = {
            "id": SystemEvents.id,
            "event_type": SystemEvents.event_type,
            "event_seen": SystemEvents.event_seen,
            "event": SystemEvents.event,
            "registration_timestamp": SystemEvents.registration_timestamp,
            "field_list": list()
        }
        # Set field option
        if fields:
            available_fields["field_list"].extend([available_fields[i] for i in fields.split(",") if i != "field_list"])
        else:
            available_fields["field_list"].extend([available_fields[i] for i in available_fields.keys() if i != "field_list"])
        system_event_object = SystemEvents.query.with_entities(
            *available_fields["field_list"]
        )
        system_event_info = system_event_object.order_by(
            desc(
                SystemEvents.registration_timestamp
            )
        )
        system_event_count = system_event_object.filter(
            SystemEvents.event_seen == 0
        ).count()
        if count:
            system_event_info = list()
        else:
            system_event_info = [i._asdict() for i in system_event_info.all()]
        return response_create({"status": "success", "data": system_event_info, "system_new_event_count": system_event_count})
    except Exception as e:
        logger.write_log(
            event_type="system-event-list",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def modify_system_events(post_form, user_ip):
    try:
        event_id = escape_chars(post_form["event_id"])
        system_event_object = SystemEvents.query.filter(
            and_(
                SystemEvents.id == event_id,
                SystemEvents.event_seen == 0
            )
        ).first()
        if system_event_object:
            system_event_object.event_seen = 1
            db.session.commit()
            log = "Event seen by '{0}'.".format(session.get("username"))
            logger.write_log(
                event_type="system-event-modify",
                event_severity="INFO",
                event_username="system",
                event_ip=user_ip,
                event=log
            )
        return response_create(
            data=dict()
        )
    except Exception as e:
        logger.write_log(
            event_type="system-event-modify",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def modify_user_password(post_form, user_ip):
    try:
        user_object = Users.query.filter(
            Users.id == session.get("user_id"),
            Users.password == calculate_hash(post_form["current_password"])
        ).first()
        if user_object:
            new_password = PasswordArgument(password=post_form["new_password"], re_password=post_form["confirm_new_password"]).play()
            user_object.password = new_password
            db.session.commit()
            log = "Password has been changed. Username: '{0}'.".format(session.get("username"))
            logger.write_log(
                event_type="user-password-modify",
                event_severity="INFO",
                event_username="system",
                event_ip=user_ip,
                event=log
            )
            return response_create({
                "status": "success",
                "message": "your password has been changed."
            })
        return response_create({
            "status": "error",
            "message": "Your current password does not match."
        })
    except Exception as e:
        logger.write_log(
            event_type="user-password-modify",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})
