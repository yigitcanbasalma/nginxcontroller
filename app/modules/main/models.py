#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import db, Base
from sqlalchemy.dialects.mysql import TINYINT, LONGTEXT


class SystemEvents(Base):
    __tablename__ = "system_events"

    event_type = db.Column(db.String(75), server_default="warning")
    event = db.Column(LONGTEXT)
    registration_timestamp = db.Column(db.DateTime, default=db.func.current_timestamp())
    event_seen = db.Column(TINYINT(1), server_default="0")

    def __init__(self, **kwargs):
        super(SystemEvents, self).__init__(**kwargs)

    def __repr__(self):
        return '<SystemEvent %r>' % self.id
