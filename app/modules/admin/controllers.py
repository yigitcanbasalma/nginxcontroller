#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import session
from os.path import join as dir_join
from sqlalchemy import and_, or_, desc, asc, func
from app import db, logger, mail_template_base, mail_manager, base_url
from app.extensions.db_ext import get_token, calculate_hash
from app.extensions.security_ext import escape_chars, strong_password, StringArgument, IPArgument, BoolStringArgument, EmailArgument, UserRoleArgument
from app.extensions.response_ext import response_create
from app.extensions.general_ext import get_user_role_name
from app.modules.models import Pages
from app.modules.auth.models import UserRoles, Users
from app.modules.admin.models import AgentAPIKeys, HostGroups


def create_agent_api_key(post_form, user_ip):
    try:
        alias = StringArgument(target=post_form["alias"].title(), data_type="Alias", min_length=2, max_length=30).play()
        ip_address_pool = IPArgument(target=post_form["ip_address_pool"]).play()
        api_key = get_token()
        api_key_hash = calculate_hash(target=api_key)
        new_agent_api_key = AgentAPIKeys(
            alias=alias,
            key_hash=api_key_hash,
            ip_address_pool=ip_address_pool
        )
        db.session.add(new_agent_api_key)
        db.session.commit()
        log = "Agent API key created. Alias: '{0}', Username: '{1}'.".format(alias, session.get("username"))
        logger.write_log(
            event_type="agent-api-key-create",
            event_severity="INFO",
            event_username="system",
            event_ip=user_ip,
            event=log
        )
        return response_create({
            "status": "success",
            "message": "You should note this key. You won't be able to access it later.",
            "agent_api_key": api_key
        })
    except Exception as e:
        logger.write_log(
            event_type="agent-api-key-create",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def list_agent_api_key_information(user_ip):
    try:
        api_key_info = AgentAPIKeys.query.with_entities(
            AgentAPIKeys.id,
            AgentAPIKeys.alias,
            AgentAPIKeys.status,
            AgentAPIKeys.registration_timestamp,
            AgentAPIKeys.ip_address_pool
        ).filter(
            AgentAPIKeys.deleted == 0
        ).order_by(
            desc(
                AgentAPIKeys.registration_timestamp
            )
        ).all()
        return response_create({"status": "success", "api_key_info": api_key_info})
    except Exception as e:
        logger.write_log(
            event_type="agent-api-key-list",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": "Something went wrong. Please try again later."})


def modify_agent_api_key(post_form, user_ip):
    try:
        ip_address_pool = IPArgument(target=post_form["ip_address_pool"]).play()
        status = BoolStringArgument(target=post_form["status"], data_type="Status").play()
        api_key_id = escape_chars(post_form["api_key_id"])
        agent_api_key_object = AgentAPIKeys.query.filter(
            and_(
                AgentAPIKeys.id == api_key_id,
                AgentAPIKeys.deleted == 0
            )
        ).first()
        if agent_api_key_object:
            is_changed = False
            changes = dict()
            if agent_api_key_object.ip_address_pool != ip_address_pool:
                is_changed = True
                changes["ip_address_pool"] = {
                    "old": agent_api_key_object.ip_address_pool,
                    "new": ip_address_pool
                }
                agent_api_key_object.ip_address_pool = ip_address_pool
            if agent_api_key_object.status != status:
                is_changed = True
                changes["status"] = {
                    "old": agent_api_key_object.status,
                    "new": status
                }
                agent_api_key_object.status = status
            if is_changed:
                db.session.commit()
                log = "Agent API key modified. Changes: '{0}', Username: '{1}'.".format("; ".join([k + ": " + str(v) for k, v in changes.items()]), session.get("username"))
                logger.write_log(
                    event_type="agent-api-key-modify",
                    event_severity="INFO",
                    event_username="system",
                    event_ip=user_ip,
                    event=log
                )
                return response_create({
                    "status": "success",
                    "message": "Agent API Key modified.",
                })
        return response_create({
            "status": "warning",
            "message": "Nothings changed."
        })
    except Exception as e:
        logger.write_log(
            event_type="agent-api-key-modify",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def delete_agent_api_key(api_key_id, user_ip):
    try:
        agent_api_key_object = AgentAPIKeys.query.filter(
            and_(
                AgentAPIKeys.id == api_key_id,
                AgentAPIKeys.deleted == 0
            )
        ).first()
        if agent_api_key_object:
            agent_api_key_object.deleted = 1
            db.session.commit()
            log = "Agent API key deleted. Username: '{0}', Alias: '{1}'.".format(session.get("username"), agent_api_key_object.alias)
            logger.write_log(
                event_type="agent-api-key-delete",
                event_severity="INFO",
                event_username="system",
                event_ip=user_ip,
                event=log
            )
            return response_create({
                "status": "success",
                "message": "API Key deleted."
            })
    except Exception as e:
        logger.write_log(
            event_type="agent-api-key-delete",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def create_host_group(post_form, user_ip):
    try:
        alias = StringArgument(target=post_form["alias"].title(), data_type="Alias", min_length=5, max_length=75).play()
        is_default = BoolStringArgument(target=int(post_form["is_default"]), data_type="Is Default").play()
        default_server_changed = False
        old_default_server_alias = None
        # Change default host group if changed
        if is_default == 1:
            old_default = HostGroups.query.filter(
                HostGroups.is_default == 1
            ).first()
            old_default.is_default = 0
            default_server_changed = True
            old_default_server_alias = old_default.alias
        # Continue
        new_host_group = HostGroups(
            alias=alias,
            is_default=is_default
        )
        db.session.add(new_host_group)
        db.session.commit()
        log = "Host group created. Alias: '{0}', Username: '{1}'.".format(alias, session.get("username"))
        if default_server_changed:
            log += " Default server changed. Old: '{0}'.".format(old_default_server_alias)
        logger.write_log(
            event_type="agent-host-group-create",
            event_severity="INFO",
            event_username="system",
            event_ip=user_ip,
            event=log
        )
        return response_create({
            "status": "success",
            "message": "Host group created."
        })
    except Exception as e:
        logger.write_log(
            event_type="agent-host-group-create",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def list_host_group_information(fields, user_ip):
    try:
        available_fields = {
            "id": HostGroups.id,
            "alias": HostGroups.alias,
            "registration_timestamp": HostGroups.registration_timestamp,
            "registered_host_count": HostGroups.registered_host_count,
            "status": HostGroups.status,
            "is_default": HostGroups.is_default,
            "field_list": list()
        }
        # Set field option
        if fields:
            available_fields["field_list"].extend([available_fields[i] for i in fields.split(",") if i != "field_list"])
        else:
            available_fields["field_list"].extend([available_fields[i] for i in available_fields.keys() if i != "field_list"])
        # Get data from database
        host_group_info = HostGroups.query.with_entities(
            *available_fields["field_list"]
        ).filter(
            HostGroups.deleted == 0
        ).order_by(
            desc(
                HostGroups.registration_timestamp
            )
        ).all()
        return response_create({"status": "success", "host_group_info": host_group_info})
    except Exception as e:
        logger.write_log(
            event_type="agent-host-group-list",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": "Something went wrong. Please try again later."})


def modify_host_group(post_form, user_ip):
    try:
        alias = StringArgument(target=post_form["alias"].title(), data_type="Alias", min_length=5, max_length=75).play()
        is_default = BoolStringArgument(target=int(post_form["is_default"]), data_type="Is Default").play()
        status = BoolStringArgument(target=post_form["status"], data_type="Status").play()
        host_group_id = escape_chars(post_form["host_group_id"])
        host_group_object = HostGroups.query.filter(
            and_(
                HostGroups.id == host_group_id,
                HostGroups.deleted == 0
            )
        ).first()
        if host_group_object:
            default_server_changed = False
            old_default_server_alias = None
            # Change default host group if changed
            if is_default == 1:
                old_default = HostGroups.query.filter(
                    HostGroups.is_default == 1
                ).first()
                old_default.is_default = 0
                default_server_changed = True
                old_default_server_alias = old_default.alias
            # Continue
            is_changed = False
            changes = dict()
            if host_group_object.alias != alias:
                is_changed = True
                changes["alias"] = {
                    "old": host_group_object.alias,
                    "new": alias
                }
                host_group_object.alias = alias
            if host_group_object.is_default != is_default:
                if host_group_object.is_default == 1:
                    return response_create({
                        "status": "error",
                        "message": "Default host group state can not be change.",
                    })
                elif is_default == 1 and host_group_object.status == "Deactive":
                    return response_create({
                        "status": "error",
                        "message": "The server whose status is offline cannot be set as default.",
                    })
                is_changed = True
                changes["is_default"] = {
                    "old": host_group_object.is_default,
                    "new": is_default
                }
                host_group_object.is_default = is_default
            if host_group_object.status != status:
                if host_group_object.is_default == 1 and status == "Deactive":
                    return response_create({
                        "status": "error",
                        "message": "Default host group status can not be change.",
                    })
                is_changed = True
                changes["status"] = {
                    "old": host_group_object.status,
                    "new": status
                }
                host_group_object.status = status
            if is_changed:
                db.session.commit()
                log = "Host group modified. Changes: '{0}', Username: '{1}'.".format("; ".join([k + ": " + str(v) for k, v in changes.items()]), session.get("username"))
                if default_server_changed:
                    log += " Default server changed. Old: '{0}'.".format(old_default_server_alias)
                logger.write_log(
                    event_type="agent-host-group-modify",
                    event_severity="INFO",
                    event_username="system",
                    event_ip=user_ip,
                    event=log
                )
                return response_create({
                    "status": "success",
                    "message": "Host group modified.",
                })
        return response_create({
            "status": "warning",
            "message": "Nothings changed."
        })
    except Exception as e:
        logger.write_log(
            event_type="agent-host-group-modify",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def delete_host_group(host_group_id, user_ip):
    try:
        host_group_object = HostGroups.query.filter(
            and_(
                HostGroups.id == host_group_id,
                HostGroups.deleted == 0
            )
        ).first()
        if host_group_object:
            if host_group_object.is_default == 1:
                return response_create({
                    "status": "error",
                    "message": "Default host group can not be deleted."
                })
            host_group_object.deleted = 1
            db.session.commit()
            log = "Host group deleted. Username: '{0}', Alias: '{1}'.".format(session.get("username"), host_group_object.alias)
            logger.write_log(
                event_type="host-group-delete",
                event_severity="INFO",
                event_username="system",
                event_ip=user_ip,
                event=log
            )
            return response_create({
                "status": "success",
                "message": "Host group deleted."
            })
    except Exception as e:
        logger.write_log(
            event_type="host-group-delete",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def create_user(post_form, user_ip):
    try:
        username = StringArgument(target=post_form["username"], data_type="Username", min_length=5, max_length=50, upper_case=True, lower_case=True, special_chars=True).play()
        first_name = StringArgument(target=post_form["first_name"].title(), data_type="FirstName", min_length=3, max_length=50, upper_case=True, lower_case=True).play()
        last_name = StringArgument(target=post_form["last_name"].upper(), data_type="LastName", max_length=50, upper_case=True).play()
        email = EmailArgument(target=post_form["email"]).play()
        user_role = UserRoleArgument(target=post_form["user_role"]).play()
        is_exist = Users.query.with_entities(
            Users.id
        ).filter(
            or_(
                Users.username == username,
                Users.email == email,
                and_(
                    Users.first_name == first_name,
                    Users.last_name == last_name
                )
            )
        ).first()
        if not is_exist:
            user_password = strong_password()
            new_user = Users(
                username=username,
                email=email,
                password=calculate_hash(target=user_password),
                first_name=first_name,
                last_name=last_name,
                role=user_role
            )
            db.session.add(new_user)
            db.session.commit()
            new_user_mail_subject = "Your account created for {0}.".format(base_url)
            new_user_mail_body = open(dir_join(mail_template_base, "new_user_account.html"), "r").read()
            new_user_mail_body = new_user_mail_body.replace("$RealName", "{0} {1}".format(new_user.first_name, new_user.last_name))
            new_user_mail_body = new_user_mail_body.replace("$UserPassword", str(user_password))
            new_user_mail_body = new_user_mail_body.replace("$BaseURL", base_url)
            # Send user mail
            mail_manager.send_mail(subject=new_user_mail_subject, body=new_user_mail_body, recipients={"to": [new_user.email]}, html=True)
            log = "User added by '{0}'. Username: '{1}', Role: '{2}'.".format(session.get("username"), username, get_user_role_name(user_role_id=user_role))
            logger.write_log(
                event_type="user-create",
                event_severity="INFO",
                event_username="system",
                event_ip=user_ip,
                event=log
            )
            return response_create({
                "status": "success",
                "message": "User added."
            })
        return response_create({
            "status": "warning",
            "message": "User already exists."
        })
    except Exception as e:
        logger.write_log(
            event_type="user-create",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def list_user_information(fields, user_ip):
    try:
        available_fields = {
            "id": Users.id,
            "username": Users.username,
            "email": Users.email,
            "first_name": Users.first_name,
            "last_name": Users.last_name,
            "role": func.get_role_name(Users.role),
            "status": Users.status,
            "registration_timestamp": Users.registration_timestamp,
            "field_list": list()
        }
        # Set field option
        if fields:
            available_fields["field_list"].extend([available_fields[i] for i in fields.split(",") if i != "field_list"])
        else:
            available_fields["field_list"].extend([available_fields[i] for i in available_fields.keys() if i != "field_list"])
        # Get data from database
        user_info = Users.query.with_entities(
            *available_fields["field_list"]
        ).filter(
            Users.deleted == 0
        ).order_by(
            asc(
                Users.registration_timestamp
            )
        ).all()
        return response_create({"status": "success", "user_info": user_info})
    except Exception as e:
        logger.write_log(
            event_type="user-list",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": "Something went wrong. Please try again later."})


def modify_user(post_form, user_ip):
    try:
        user_id = escape_chars(post_form["user_id"])
        user_role = UserRoleArgument(target=post_form["user_role"]).play()
        status = BoolStringArgument(target=post_form["status"], data_type="Status").play()
        user_object = Users.query.filter(
            and_(
                Users.id == user_id,
                Users.id != session.get("user_id"),
                Users.deleted == 0,
                Users.protected == 0
            )
        ).first()
        if user_object:
            is_changed = False
            changes = dict()
            if user_object.status != status:
                is_changed = True
                changes["status"] = {
                    "old": user_object.status,
                    "new": status
                }
                user_object.status = status
            if user_object.role != user_role:
                is_changed = True
                changes["user_role"] = {
                    "old": get_user_role_name(user_role_id=user_object.role),
                    "new": get_user_role_name(user_role_id=user_role)
                }
                user_object.role = user_role
            if is_changed:
                db.session.commit()
                log = "User modified by '{0}'. Username: '{1}', Changes: '{2}'.".format(session.get("username"), user_object.username, "; ".join([k + ": " + str(v) for k, v in changes.items()]))
                logger.write_log(
                    event_type="user-modify",
                    event_severity="INFO",
                    event_username="system",
                    event_ip=user_ip,
                    event=log
                )
                return response_create({
                    "status": "success",
                    "message": "User modified."
                })
            return response_create({
                "status": "warning",
                "message": "Nothings changed."
            })
        return response_create({
            "status": "error",
            "message": "User does not exist or protected account."
        })
    except Exception as e:
        logger.write_log(
            event_type="user-modify",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": "Something went wrong. Please try again later."})


def delete_user(user_id, user_ip):
    try:
        user_object = Users.query.filter(
            and_(
                Users.id == user_id,
                Users.deleted == 0,
                Users.protected == 0
            )
        ).first()
        if user_object:
            user_object.deleted = 1
            db.session.commit()
            log = "User deleted by '{0}'. Username: '{1}'.".format(session.get("username"), user_object.username)
            logger.write_log(
                event_type="user-delete",
                event_severity="INFO",
                event_username="system",
                event_ip=user_ip,
                event=log
            )
            return response_create({
                "status": "success",
                "message": "User deleted."
            })
        return response_create({
            "status": "error",
            "message": "User does not exist or protected account."
        })
    except Exception as e:
        logger.write_log(
            event_type="user-delete",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})


def list_user_role_information(fields, user_ip):
    try:
        available_fields = {
            "id": UserRoles.id,
            "role_name": UserRoles.role_name,
            "registration_timestamp": UserRoles.registration_timestamp,
            "field_list": list()
        }
        # Set field option
        if fields:
            available_fields["field_list"].extend([available_fields[i] for i in fields.split(",") if i != "field_list"])
        else:
            available_fields["field_list"].extend([available_fields[i] for i in available_fields.keys() if i != "field_list"])
        # Get data from database
        user_role_info = UserRoles.query.with_entities(
            *available_fields["field_list"]
        ).order_by(
            desc(
                UserRoles.id
            )
        ).all()
        return response_create({"status": "success", "user_role_info": user_role_info})
    except Exception as e:
        logger.write_log(
            event_type="user-role-list",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": "Something went wrong. Please try again later."})


def list_page_information(fields, user_ip):
    try:
        available_fields = {
            "id": Pages.id,
            "name": Pages.name,
            "location": Pages.location,
            "access_groups": Pages.who_is_see,
            "status": Pages.status,
            "field_list": list()
        }
        # Set field option
        if fields:
            available_fields["field_list"].extend([available_fields[i] for i in fields.split(",") if i != "field_list"])
        else:
            available_fields["field_list"].extend([available_fields[i] for i in available_fields.keys() if i != "field_list"])
        # Get data from database
        pages_object = Pages.query.with_entities(
            *available_fields["field_list"]
        ).order_by(
            asc(
                Pages.id
            )
        ).all()
        pages_list = [pg._asdict() for pg in pages_object]
        if fields and "access_groups" in fields:
            for i in range(len(pages_list)):
                pages_list[i]["access_group_object"] = [(int(e), get_user_role_name(int(e))) for e in pages_list[i]["who_is_see"].split(";")]
        return response_create({"status": "success", "page_info": pages_list})
    except Exception as e:
        logger.write_log(
            event_type="page-list",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": "Something went wrong. Please try again later."})
