#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Blueprint, render_template, request
from app.extensions.wrappers import before_request_processing, login_required
from app.modules.admin.controllers import create_agent_api_key, list_agent_api_key_information, modify_agent_api_key, delete_agent_api_key, create_host_group, list_host_group_information, modify_host_group, delete_host_group, create_user, list_user_information, modify_user, delete_user, list_user_role_information, list_page_information

admin_router = Blueprint("admin_router", __name__, url_prefix="/admin")


@admin_router.route("/settings", methods=["GET"])
@login_required
@before_request_processing(restricted=False)
def settings_page():
    return render_template(
        "admin/settings.html"
    )


@admin_router.route("/agent/api-key/create", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def create_agent_api_key_func():
    return create_agent_api_key(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/agent/api-key/list", methods=["GET"])
@login_required
@before_request_processing(restricted=True)
def list_agent_api_key_func():
    return list_agent_api_key_information(
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/agent/api-key/modify", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def modify_agent_api_key_func():
    return modify_agent_api_key(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/agent/api-key/delete/<key_id>", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def delete_agent_api_key_func(key_id):
    return delete_agent_api_key(
        api_key_id=key_id,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/agent/host-group/create", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def create_host_group_func():
    return create_host_group(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/agent/host-group/list", methods=["GET"])
@login_required
@before_request_processing(restricted=True)
def list_host_group_func():
    return list_host_group_information(
        fields=request.args.get("fields", None),
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/agent/host-group/modify", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def modify_host_group_func():
    return modify_host_group(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/agent/host-group/delete/<host_group_id>", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def delete_host_group_func(host_group_id):
    return delete_host_group(
        host_group_id=host_group_id,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/users/user/create", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def create_user_func():
    return create_user(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/users/user/list", methods=["GET"])
@login_required
@before_request_processing(restricted=True)
def list_user_func():
    return list_user_information(
        fields=request.args.get("fields", None),
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/users/user/modify", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def modify_user_func():
    return modify_user(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/users/user/delete/<user_id>", methods=["POST"])
@login_required
@before_request_processing(restricted=True)
def delete_user_func(user_id):
    return delete_user(
        user_id=user_id,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/users/user-role/list", methods=["GET"])
@login_required
@before_request_processing(restricted=True)
def list_user_role_func():
    return list_user_role_information(
        fields=request.args.get("fields", None),
        user_ip=request.headers.get("X-Forwarded-For")
    )


@admin_router.route("/pages/list", methods=["GET"])
@login_required
@before_request_processing(restricted=True)
def list_page_func():
    return list_page_information(
        fields=request.args.get("fields", None),
        user_ip=request.headers.get("X-Forwarded-For")
    )
