#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import db, Base
from sqlalchemy.dialects.mysql import TINYINT, LONGTEXT
from app.extensions.db_ext import get_uuid


class AgentAPIKeys(Base):
    __tablename__ = "agent_api_keys"

    id = db.Column(db.String(12), primary_key=True)
    alias = db.Column(db.String(30), unique=True)
    key_hash = db.Column(db.String(75), nullable=False)
    ip_address_pool = db.Column(LONGTEXT)
    registration_timestamp = db.Column(db.DateTime, default=db.func.current_timestamp())
    status = db.Column(db.String(15), server_default="Active")
    deleted = db.Column(TINYINT(1), server_default="0")

    def __init__(self, **kwargs):
        self.id = get_uuid()
        super(AgentAPIKeys, self).__init__(**kwargs)

    def __repr__(self):
        return '<AgentAPIKey %r>' % self.id


class HostGroups(Base):
    __tablename__ = "host_groups"

    id = db.Column(db.String(12), primary_key=True)
    alias = db.Column(db.String(75), unique=True)
    registered_host_count = db.Column(db.Integer, server_default="0")
    is_default = db.Column(TINYINT(1), server_default="0")
    registration_timestamp = db.Column(db.DateTime, default=db.func.current_timestamp())
    status = db.Column(db.String(15), server_default="Active")
    deleted = db.Column(TINYINT(1), server_default="0")

    def __init__(self, **kwargs):
        self.id = get_uuid()
        super(HostGroups, self).__init__(**kwargs)

    def __repr__(self):
        return '<HostGroup %r>' % self.id
