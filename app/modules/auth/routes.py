#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Blueprint, request, abort
from app import re_captcha
from app.extensions.wrappers import before_request_processing, login_required
from app.modules.auth.controllers import login, logout

auth_router = Blueprint("auth_router", __name__, url_prefix="/auth")


@auth_router.route("/login", methods=["POST"])
@before_request_processing(restricted=False)
def user_login():
    if not re_captcha.verify(remote_ip=request.headers.get("X-Forwarded-For")):
        abort(400)
    return login(
        post_form=request.form,
        user_ip=request.headers.get("X-Forwarded-For")
    )


@auth_router.route("/logout", methods=["GET"])
@login_required
def user_logout():
    return logout()
