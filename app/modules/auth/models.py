#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import db, Base
from sqlalchemy.dialects.mysql import NVARCHAR, LONGTEXT, TINYINT
from app.extensions.db_ext import get_uuid


class Users(Base):
    __tablename__ = "users"

    id = db.Column(db.String(12), primary_key=True)
    email = db.Column(db.String(200), nullable=False, unique=True)
    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(75), nullable=False)
    first_name = db.Column(NVARCHAR(50), nullable=False)
    last_name = db.Column(NVARCHAR(50), nullable=False)
    registration_timestamp = db.Column(db.DateTime, default=db.func.current_timestamp())
    role = db.Column(TINYINT(1), server_default="1")
    status = db.Column(db.String(15), server_default="Active")
    deleted = db.Column(TINYINT(1), server_default="0")
    protected = db.Column(TINYINT(1), server_default="0")

    def __init__(self, **kwargs):
        self.id = get_uuid()
        super(Users, self).__init__(**kwargs)

    def __repr__(self):
        return '<User %r>' % self.id


class UserRoles(Base):
    __tablename__ = "user_roles"

    role_name = db.Column(db.String(75), nullable=False)
    permissions = db.Column(LONGTEXT, nullable=False)
    content_hash = db.Column(db.String(75), unique=True)
    registration_timestamp = db.Column(db.DateTime, default=db.func.current_timestamp())

    def __init__(self, **kwargs):
        super(UserRoles, self).__init__(**kwargs)

    def __repr__(self):
        return '<UserRole %r>' % self.id
