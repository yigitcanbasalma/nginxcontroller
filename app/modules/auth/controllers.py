#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import session, redirect, url_for
from sqlalchemy import and_, or_
from app import logger
from app.extensions.security_ext import escape_chars
from app.extensions.db_ext import calculate_hash
from app.extensions.response_ext import response_create
from app.extensions.general_ext import get_user_role_name
from app.modules.auth.models import Users
from app.modules.models import Pages


def force_quit():
    session.clear()
    return redirect(url_for("router.index_page", account_status_changed_sign=True))


def logout():
    session.clear()
    return redirect(url_for("router.index_page"))


def login(post_form, user_ip):
    try:
        user_email_or_username = escape_chars(post_form["user_email_or_username"])
        user_password = escape_chars(post_form["password"])
        user_info = Users.query.filter(
            and_(
                or_(
                    Users.username == user_email_or_username,
                    Users.email == user_email_or_username
                ),
                Users.password == calculate_hash(target=user_password),
                Users.status == "Active"
            )
        ).first()
        if user_info:
            user_pages = Pages.query.with_entities(
                Pages.name,
                Pages.location,
                Pages.icon
            ).filter(
                and_(
                    Pages.who_is_see.like("%{0}%".format(user_info.role)),
                    Pages.status == "Active"
                )
            ).all()
            session_environments = {
                "logged-in": True,
                "user_id": user_info.id,
                "username": user_info.username,
                "email": user_info.email,
                "first_name": user_info.first_name.capitalize(),
                "last_name": user_info.last_name.upper(),
                "role": get_user_role_name(user_role_id=user_info.role),
                "role_id": user_info.role,
                "pages": user_pages
            }
            for k, v in session_environments.items():
                session[k] = v
            log = "Successfully login. Username: '{0}'.".format(user_email_or_username)
            logger.write_log(
                event_type="customer-login",
                event_severity="INFO",
                event_username="system",
                event_ip=user_ip,
                event=log
            )
            return response_create({"status": "success", "location": "/main/dashboard"})
        log = "Suspicious behaviour. Username: '{0}'".format(user_email_or_username)
        logger.write_log(
            event_type="user-login-brute-force",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=log
        )
        return response_create({"status": "error", "message": "Wrong e-mail/password."})
    except Exception as e:
        logger.write_log(
            event_type="user-login",
            event_severity="ERROR",
            event_username="system",
            event_ip=user_ip,
            event=e
        )
        return response_create({"status": "error", "message": str(e)})
