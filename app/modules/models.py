#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import db, Base
from sqlalchemy.dialects.mysql import LONGTEXT, TINYINT


class Config(Base):
    __tablename__ = "config"

    config_key = db.Column(db.String(255))
    config_value = db.Column(LONGTEXT)
    content_hash = db.Column(db.String(75), unique=True)

    def __init__(self, **kwargs):
        super(Config, self).__init__(**kwargs)

    def __repr__(self):
        return '<Config %r>' % self.id


class Pages(Base):
    __tablename__ = "pages"

    name = db.Column(db.String(50), unique=True)
    location = db.Column(db.String(255), unique=True)
    who_is_see = db.Column(LONGTEXT)
    icon = db.Column(db.String(50), unique=True)
    status = db.Column(db.String(15), server_default="Active")
    deleted = db.Column(TINYINT(1), server_default="0")

    def __init__(self, **kwargs):
        super(Pages, self).__init__(**kwargs)

    def __repr__(self):
        return '<Page %r>' % self.id
