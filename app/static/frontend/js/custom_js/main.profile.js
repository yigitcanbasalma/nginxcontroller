function modify_user_password(form_id) {
	var form = $("#"+form_id);
    $.ajax({
        type: $(form).attr("method"),
        url: $(form).attr("action"),
        data: $(form).serialize(),
        dataType: "json",
        success: function(data) {
            swal({
                position: 'top-end',
                icon: data.status,
                title: data.message,
                timer: 3000
            });
        }
    });
}