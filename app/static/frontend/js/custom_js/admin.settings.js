function create_agent_api_key(form_id) {
    var form = $("#"+form_id);
    $.ajax({
        type: $(form).attr("method"),
        url: $(form).attr("action"),
        data: $(form).serialize(),
        dataType: "json",
        success: function(data) {
            list_agent_api_key_information();
            if (data.status == "success") {
                $("#api-key").text(data.agent_api_key);
                $("#success-message-note").text(data.message);
                $("#success-message").css("display", "block");
            } else {
                swal({
                    position: 'top-end',
                    icon: "error",
                    title: data.message,
                    timer: 10000
                });
            }
        }
    });
}

function list_agent_api_key_information() {
    $.ajax({
        type: "GET",
        url: "/admin/agent/api-key/list",
        dataType: "json",
        success: function(data) {
            if (data.status == "success") {
                $("#agent-api-key-information-table tbody tr").remove();
                var content_table_body = "";
                $.each(data.api_key_info, function( index, value ) {
                    var is_active = "green";
                    if (value[2] != "Active") {
                        is_active = "red";
                    }
                    content_table_body += "<tr id='"+value[0]+"' onclick='open_modify_agent_api_key(\""+value[0]+"\",\""+value[4]+"\",\""+value[1]+"\",\""+value[2]+"\")'>";
                    content_table_body += " <td name='api_key_id'>"+value[0]+"</td>";
                    content_table_body += " <td name='api_key_alias'>"+value[1]+"</td>";
                    content_table_body += " <td name='api_key_created_at'>"+value[3]+"</td>";
                    content_table_body += " <td name='api_key_status' style='color: "+is_active+";'>"+value[2]+"</td>";
                    content_table_body += "</tr>"
                });
                $(content_table_body).appendTo("#agent-api-key-information-table tbody");
            } else {
                swal({
                    position: 'top-end',
                    icon: "error",
                    title: data.message,
                    timer: 10000
                });
            }
        }
    });
}

function open_modify_agent_api_key(tr_id, ip_address_pool, alias, status) {
    $("#agent-api-key-modify-label label[name='alias']").text(alias);
    $("#agent-api-key-modify-form input[name='ip_address_pool']").tagsinput("add", ip_address_pool);
    $("#agent-api-key-modify-form input[name='api_key_id']").val(tr_id);
    $("#agent-api-key-modify-form select[name='status']").val(status);
    $('.selectpicker').selectpicker('refresh');
    $("#agent-api-key-modify").modal("show");
}

function modify_agent_api_key(form_id) {
    var form = $("#"+form_id);
    $.ajax({
        type: $(form).attr("method"),
        url: $(form).attr("action"),
        data: $(form).serialize(),
        dataType: "json",
        success: function(data) {
            list_agent_api_key_information();
            swal({
                position: 'top-end',
                icon: data.status,
                title: data.message,
                timer: 3000
            });
        }
    });
}

function delete_agent_api_key() {
    var key_name = $("#agent-api-key-modify-label label[name='alias']").text();
    if (confirm("'"+key_name+"' named API key will be delete. Are you sure?")) {
        var key_id = $("#agent-api-key-modify-form input[name='api_key_id']").val();
        $.ajax({
            type: "POST",
            url: "/admin/agent/api-key/delete/"+key_id,
            dataType: "json",
            success: function(data) {
                list_agent_api_key_information();
                swal({
                    position: 'top-end',
                    icon: data.status,
                    title: data.message,
                    timer: 3000
                });
            }
        });
    }
}

function create_host_group(form_id) {
    var form = $("#"+form_id);
    $.ajax({
        type: $(form).attr("method"),
        url: $(form).attr("action"),
        data: $(form).serialize(),
        dataType: "json",
        success: function(data) {
            list_host_group_information();
            swal({
                position: 'top-end',
                icon: data.status,
                title: data.message,
                timer: 3000
            });
        }
    });
}

function list_host_group_information() {
    $.ajax({
        type: "GET",
        url: "/admin/agent/host-group/list?fields=id,alias,registration_timestamp,registered_host_count,status,is_default",
        dataType: "json",
        success: function(data) {
            if (data.status == "success") {
                $("#host-group-information-table tbody tr").remove();
                var content_table_body = "";
                $.each(data.host_group_info, function( index, value ) {
                    var is_active = "green";
                    var is_default_badge = "";
                    if (value[4] != "Active") {
                        is_active = "red";
                    }
                    if (value[5] == 1) {
                        is_default_badge = "<span class='badge badge-secondary'>default</span>"
                    }
                    content_table_body += "<tr id='"+value[0]+"' onclick='open_modify_host_group(\""+value[0]+"\",\""+value[1]+"\",\""+value[4]+"\",\""+value[5]+"\")'>";
                    content_table_body += " <td name='host_group_id'>"+value[0]+"</td>";
                    content_table_body += " <td name='host_group_alias'>"+value[1]+" "+is_default_badge+"</td>";
                    content_table_body += " <td name='host_group_created_at'>"+value[2]+"</td>";
                    content_table_body += " <td name='host_group_registered_host_count'>"+value[3]+"</td>";
                    content_table_body += " <td name='host_group_status' style='color: "+is_active+";'>"+value[4]+"</td>";
                    content_table_body += "</tr>"
                });
                $(content_table_body).appendTo("#host-group-information-table tbody");
            } else {
                swal({
                    position: 'top-end',
                    icon: "error",
                    title: data.message,
                    timer: 10000
                });
            }
        }
    });
}

function open_modify_host_group(tr_id, alias, status, is_default) {
    $("#host-group-modify-label label[name='alias']").text(alias);
    $("#host-group-modify-form input[name='alias']").val(alias);
    $("#host-group-modify-form input[name='host_group_id']").val(tr_id);
    $("#host-group-modify-form select[name='status']").val(status);
    $("#host-group-modify-form select[name='is_default']").val(is_default);
    $('.selectpicker').selectpicker('refresh');
    $("#host-group-modify").modal("show");
}

function modify_host_group(form_id) {
    var form = $("#"+form_id);
    $.ajax({
        type: $(form).attr("method"),
        url: $(form).attr("action"),
        data: $(form).serialize(),
        dataType: "json",
        success: function(data) {
            list_host_group_information();
            swal({
                position: 'top-end',
                icon: data.status,
                title: data.message,
                timer: 3000
            });
        }
    });
}

function delete_host_group() {
    var host_group_name = $("#host-group-modify-label label[name='alias']").text();
    if (confirm("'"+host_group_name+"' named host group will be delete. Are you sure?")) {
        var host_group_id = $("#host-group-modify-form input[name='host_group_id']").val();
        $.ajax({
            type: "POST",
            url: "/admin/agent/host-group/delete/"+host_group_id,
            dataType: "json",
            success: function(data) {
                list_host_group_information();
                swal({
                    position: 'top-end',
                    icon: data.status,
                    title: data.message,
                    timer: 3000
                });
            }
        });
    }
}

function open_user_create() {
    $.ajax({
        type: "GET",
        url: "/admin/users/user-role/list?fields=id,role_name",
        dataType: "json",
        success: function(data) {
            $("#user-create-form select[name='user_role'] option:not(:selected)").remove();
            var option = "";
            for ( var index = 0; data.user_role_info.length > index; index++ ) {
                option += "<option value='"+data.user_role_info[index][0]+"'>"+data.user_role_info[index][1]+"</option>";
            }
            $(option).appendTo("#user-create-form select[name='user_role']");
            $('.selectpicker').selectpicker('refresh');
        }
    });
    $("#user-create").modal("show");
}

function create_user(form_id) {
    var form = $("#"+form_id);
    $(".loading-image").show();
    $.ajax({
        type: $(form).attr("method"),
        url: $(form).attr("action"),
        data: $(form).serialize(),
        dataType: "json",
        success: function(data) {
            $(".loading-image").hide();
            list_user_information();
            swal({
                position: 'top-end',
                icon: data.status,
                title: data.message,
                timer: 3000
            });
        }
    });
}

function list_user_information() {
    $.ajax({
        type: "GET",
        url: "/admin/users/user/list?fields=id,username,email,first_name,last_name,registration_timestamp,role,status",
        dataType: "json",
        success: function(data) {
            if (data.status == "success") {
                $("#user-information-table tbody tr").remove();
                var content_table_body = "";
                $.each(data.user_info, function( index, value ) {
                    var is_active = "green";
                    if (value[7] != "Active") {
                        is_active = "red";
                    }
                    content_table_body += "<tr id='"+value[0]+"' onclick='open_modify_user(\""+value[0]+"\",\""+value[1]+"\",\""+value[6]+"\",\""+value[7]+"\")'>";
                    content_table_body += " <td name='user_id'>"+value[0]+"</td>";
                    content_table_body += " <td name='user_username'>"+value[1]+"</td>";
                    content_table_body += " <td name='user_email'>"+value[2]+"</td>";
                    content_table_body += " <td name='user_first_name'>"+value[3]+"</td>";
                    content_table_body += " <td name='user_last_name'>"+value[4]+"</td>";
                    content_table_body += " <td name='user_registration_timestamp'>"+value[5]+"</td>";
                    content_table_body += " <td name='user_role'>"+value[6]+"</td>";
                    content_table_body += " <td name='user_status' style='color: "+is_active+";'>"+value[7]+"</td>";
                    content_table_body += "</tr>"
                });
                $(content_table_body).appendTo("#user-information-table tbody");
            } else {
                swal({
                    position: 'top-end',
                    icon: "error",
                    title: data.message,
                    timer: 10000
                });
            }
        }
    });
}

function open_modify_user(tr_id, username, role, status) {
    $.ajax({
        type: "GET",
        url: "/admin/users/user-role/list?fields=id,role_name",
        dataType: "json",
        success: function(data) {
            $("#user-modify-form select[name='user_role'] option:not(:selected)").remove();
            var option = "";
            for ( var index = 0; data.user_role_info.length > index; index++ ) {
                if (data.user_role_info[index][1] == role) {
                    option += "<option selected='selected' value='"+data.user_role_info[index][0]+"'>"+data.user_role_info[index][1]+"</option>";
                } else {
                    option += "<option value='"+data.user_role_info[index][0]+"'>"+data.user_role_info[index][1]+"</option>";
                }
                
            }
            $(option).appendTo("#user-modify-form select[name='user_role']");
            $('.selectpicker').selectpicker('refresh');
        }
    });
    $("#user-modify-label label[name='username']").text(username);
    $("#user-modify-form select[name='status']").val(status);
    $("#user-modify-form input[name='user_id']").val(tr_id);
    $('.selectpicker').selectpicker('refresh');
    $("#user-modify").modal("show");
}

function modify_user(form_id) {
    var form = $("#"+form_id);
    $(".loading-image").show();
    $.ajax({
        type: $(form).attr("method"),
        url: $(form).attr("action"),
        data: $(form).serialize(),
        dataType: "json",
        success: function(data) {
            $(".loading-image").hide();
            list_user_information();
            swal({
                position: 'top-end',
                icon: data.status,
                title: data.message,
                timer: 3000
            });
        }
    });
}

function delete_user() {
    var username = $("#user-modify-label label[name='username']").text();
    if (confirm("'"+username+"' named user will be delete. Are you sure?")) {
        var user_id = $("#user-modify-form input[name='user_id']").val();
        $.ajax({
            type: "POST",
            url: "/admin/users/user/delete/"+user_id,
            dataType: "json",
            success: function(data) {
                list_user_information();
                swal({
                    position: 'top-end',
                    icon: data.status,
                    title: data.message,
                    timer: 3000
                });
            }
        });
    }
}

function list_page_information() {
    $.ajax({
        type: "GET",
        url: "/admin/pages/list?fields=id,name,location,access_groups,status",
        dataType: "json",
        success: function(data) {
            if (data.status == "success") {
                $("#pages-information-table tbody tr").remove();
                var content_table_body = "";
                $.each(data.page_info, function( index, value ) {
                    var is_active = "green";
                    var access_groups = [];
                    if (value.status != "Active") {
                        is_active = "red";
                    }
                    for (var i=0; value.access_group_object.length > i; i++) {
                        access_groups.push(value.access_group_object[i][1]);
                    }
                    content_table_body += "<tr id='"+value.id+"' onclick='open_modify_page(\""+value.id+"\",\""+value.name+"\")'>";
                    content_table_body += " <td>"+value.id+"</td>";
                    content_table_body += " <td>"+value.name+"</td>";
                    content_table_body += " <td>"+value.location+"</td>";
                    content_table_body += " <td>"+access_groups.join(", ")+"</td>";
                    content_table_body += " <td name='page_status' style='color: "+is_active+";'>"+value.status+"</td>";
                    content_table_body += "</tr>"
                });
                $(content_table_body).appendTo("#pages-information-table tbody");
            } else {
                swal({
                    position: 'top-end',
                    icon: "error",
                    title: data.message,
                    timer: 10000
                });
            }
        }
    });
}