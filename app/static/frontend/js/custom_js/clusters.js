function list_hosts_information() {
    $.ajax({
        type: "GET",
        url: "/clusters/hosts/list",
        dataType: "json",
        success: function(data) {
            if (data.status == "success") {
            	if (data.hosts_info.length == 0) {
            		swal({
	                    position: 'top-end',
	                    icon: "warning",
	                    title: "You don't have any cluster.",
	                    timer: 10000
	                });
	                return;
            	}
            	// Clear all nav tabs
                $("#clusters-nav-tabs li").remove();
                $("#nav-tabs-area div").remove();
                // Build news
                var tab_content = "";
                var li_object = "";
                $.each(data.hosts_info, function( key, value ) {
                	var host_group_id = "";
                	var main_content = "";
                	// Set the sub objects
                	for ( var index = 0; value.length > index; index++ ) {
                		var is_active = "green";
                		var is_running = "green";
                		var is_running_text = "Up";
                        var tags = [];
                        if (value[index][9]) {
                            var tags = value[index][9].split(";");
                        }
                		if (value[index][8] == "Deactive") {
                			is_active = "red";
                		}
                		if (value[index][4] == 0) {
                			is_running = "red";
                			is_running_text = "Down";
                		}
                		host_group_id = value[index][7];
						main_content += "			<div class='col-md-6'>";
						main_content += "				<div class='box'>";
						main_content += "					<div class='row'>";
						main_content += "						<div class='col-md-12'>";
						main_content += "							<label class='clusters-header'>"+value[index][1]+"</label> &nbsp; <label>"+value[index][2]+"</label> &nbsp; <span class='badge badge-dark'>"+value[index][3]+"</span> <span style='float: right; cursor: pointer;' data-feather='settings' onclick=\"open_modify_host(\'"+value[index][0]+"\',\'"+value[index][1]+"\',\'"+host_group_id+"\',\'"+value[index][8]+"\')\"></span><span style='float: right; margin-right: 3px; cursor: pointer;' data-feather='info' onclick=\"open_info_host(\'"+value[index][1]+"\',\'"+value[index][0]+"\')\"></span>";
						main_content += "						</div>";
						main_content += "						<div class='col-md-12'>";
						main_content += "							<label style='color: "+is_active+";'>"+value[index][8]+"</label> <label style='color: "+is_running+";'>"+is_running_text+"</label> / <label>"+value[index][5]+" &nbsp; <label>(nginx "+value[index][6]+")</label></label>";
						main_content += "						</div>";
						main_content += "						<div class='col-md-12'>";
						main_content += "							<label class='clusters-header'>Tags:";
						for ( var i = 0; tags.length > i; i++ ) {
							main_content += "<span style='margin-left: 2px;' class='badge badge-secondary'>"+tags[i]+"</span>";
						} 
						main_content += "</label>";
						main_content += "						</div>";
						main_content += "					</div>";
						main_content += "				</div>";
						main_content += "			</div>";
                	}
                	tab_content += "<div class='tab-pane fade' id='host-group-"+host_group_id+"' role='tabpanel' aria-labelledby='host-group-tab-"+host_group_id+"'>";
                	tab_content += "	<div class='row'>";
                	tab_content += main_content;
                	tab_content += "	</div>";
                	tab_content += "</div>";
                	li_object += "<li class='nav-item'><a class='nav-link' id='host-group-tab-"+host_group_id+"' data-toggle='tab' href='#host-group-"+host_group_id+"' role='tab' aria-controls='"+host_group_id+"-host-group' aria-selected='false'>"+key+"</a>";
                })
                $(li_object).appendTo("#clusters-nav-tabs");
                $(tab_content).appendTo("#nav-tabs-area");
                $('#clusters-nav-tabs li:first-child a').tab('show');
                feather.replace();
            } else {
                swal({
                    position: 'top-end',
                    icon: "error",
                    title: data.message,
                    timer: 10000
                });
            }
        }
    });
}

function open_modify_host(host_id, hostname, host_group_id, status) {
	$.ajax({
        type: "GET",
        url: "/admin/agent/host-group/list?fields=id,alias",
        dataType: "json",
        success: function(data) {
        	$("#host-modify-form select[name='host_group'] option:not(:selected)").remove();
        	var option = "";
        	for ( var index = 0; data.host_group_info.length > index; index++ ) {
        		if (data.host_group_info[index][0] == host_group_id){
        			option += "<option value='"+data.host_group_info[index][0]+"' selected='selected'>"+data.host_group_info[index][1]+"</option>";
        		} else {
        			option += "<option value='"+data.host_group_info[index][0]+"'>"+data.host_group_info[index][1]+"</option>";
        		}
        	}
        	$(option).appendTo("#host-modify-form select[name='host_group']");
        	$('.selectpicker').selectpicker('refresh');
        }
    });
    $("#host-modify-label label[name='hostname']").text(hostname);
    $("#host-modify-form input[name='host_id']").val(host_id);
    $("#host-modify-form select[name='status']").val(status);
    $('.selectpicker').selectpicker('refresh');
    $("#host-modify").modal("show");
}

function modify_host(form_id) {
	var form = $("#"+form_id);
    $.ajax({
        type: $(form).attr("method"),
        url: $(form).attr("action"),
        data: $(form).serialize(),
        dataType: "json",
        success: function(data) {
            list_hosts_information();
            swal({
                position: 'top-end',
                icon: data.status,
                title: data.message,
                timer: 3000
            });
        }
    });
}

function delete_host() {
    var hostname = $("#host-modify-label label[name='hostname']").text();
    if (confirm("'"+hostname+"' named host will be delete. Are you sure?")) {
        var host_id = $("#host-modify-form input[name='host_id']").val();
        $.ajax({
            type: "POST",
            url: "/clusters/hosts/delete/"+host_id,
            dataType: "json",
            success: function(data) {
                list_hosts_information();
                swal({
                    position: 'top-end',
                    icon: data.status,
                    title: data.message,
                    timer: 5000
                });
            }
        });
    }
}

function open_info_host(hostname, host_id) {
    $("#host-info-label label[name='hostname']").text(hostname);
    $.ajax({
        type: "GET",
        url: "/clusters/hosts/host-info/"+host_id,
        dataType: "json",
        success: function(data) {
        	var h = data.host_info;
        	var status_color = "green";
        	var nginx_running_status = "green";
        	var nginx_running_text = "Running";
        	var stub_status_color = "green";
        	var stub_status_text = "True";
        	var built_from_source_color = "green";
        	var built_from_source_text = "True";
        	if (h.status == "Deactive") {
        		status_color = "red";
        	}
        	if (h.stub_status_enabled == 0) {
        		stub_status_color = "red";
        		stub_status_text = "False";
        	}
        	if (h.built_from_source == 0) {
        		built_from_source_color = "red";
        		built_from_source_text = "False";
        	}
        	if (h.running == 0) {
        		nginx_running_status = "red";
        		nginx_running_text = "Not Running";
        	}
        	// Set lists
        	var tags = h.tags.split(";");
        	var configure_args = h.build_config.split(";");
        	// Set colors
        	$("#info-tab-content label[name='sysobj-status']").css("color", status_color);
        	$("#info-tab-content label[name='ngobj-status']").css("color", nginx_running_status);
        	$("#info-tab-content label[name='ngobj-stub-status-enabled']").css("color", stub_status_color);
        	$("#info-tab-content label[name='ngobj-built-from-source']").css("color", built_from_source_color);
        	// Truncate old areas
        	$("#info-tab-content label[name='ngobj-tags'] span").remove();
        	$("#info-tab-content #configure-args-area p").remove();
        	// System Objects
        	// > General
        	$("#info-tab-content label[name='sysobj-hostname']").text(h.hostname);
        	$("#info-tab-content label[name='sysobj-status']").text(h.status);
        	$("#info-tab-content label[name='sysobj-last-seen']").text(h.last_update);
        	$("#info-tab-content label[name='sysobj-created']").text(h.registration_timestamp);
        	$("#info-tab-content label[name='sysobj-host-group']").text(h.host_group_name);
        	// > Operating system
        	$("#info-tab-content label[name='sysobj-os-version']").text(h.os_info);
        	$("#info-tab-content label[name='sysobj-os-uptime']").text(h.host_boot_time);
        	// > Hardware
        	$("#info-tab-content label[name='sysobj-hypervisor']").text(h.hypervisor);
        	// Nginx Objects
        	// > General
        	$("#info-tab-content label[name='ngobj-status']").text(nginx_running_text);
        	$("#info-tab-content label[name='ngobj-version']").text(h.nginx_package_version);
        	$("#info-tab-content label[name='ngobj-agent-version']").text(h.agent_version);
        	$("#info-tab-content label[name='ngobj-agent-uuid']").text(h.host_uuid);
        	$("#info-tab-content label[name='ngobj-stub-status-enabled']").text(stub_status_text);
        	$("#info-tab-content label[name='ngobj-built-from-source']").text(built_from_source_text);
        	for ( var i = 0; tags.length > i; i++ ) {
				$("<span style='margin-left: 2px;' class='badge badge-secondary'>"+tags[i]+"</span>").appendTo("#info-tab-content label[name='ngobj-tags']");
			}
			// > Configure args
			for ( var i = 0; configure_args.length > i; i++ ) {
				$("<p class='item'><label class='item'> --"+configure_args[i]+"</label></p>").appendTo("#info-tab-content #configure-args-area");
			}
        }
    });
    $("#host-info").modal("show");
}

function go_to_cluster_configuration() {
    var active_tab_name = $("#clusters-nav-tabs a.active").text();
    window.location = "/clusters/cluster/lb/configuration/"+active_tab_name;
}

function modify_cluster_config(cluster_id) {
    var cluster_name = $("label.cluster-name").text();
    if (confirm("'"+cluster_name+"' named cluster's configuration will be update. Are you sure?")) {
        var editor = ace.edit("editor-"+cluster_id);
        var config_content = editor.getValue();
        var cluster_id = $("#host_group_id").val();
        var data = {"host_group_id": cluster_id, "configuration": config_content, "config_id": cluster_id};
        $.ajax({
            type: "POST",
            url: "/clusters/cluster/lb/configuration/modify",
            dataType: "json",
            data: data,
            success: function(data) {
                swal({
                    position: 'top-end',
                    icon: data.status,
                    title: data.message,
                    timer: 5000
                });
            }
        });
    }
}