function sign_in(form_id) {
    var form = $("#"+form_id);
    $.ajax({
        type: $(form).attr("method"),
        url: $(form).attr("action"),
        data: $(form).serialize(),
        dataType: "json",
        success: function(data) {
            if (data.status == "success") {
                window.location.href = data.location;
            } else {
                swal({
                    position: 'top-end',
                    icon: "error",
                    title: data.message,
                    timer: 5000
                });
            }
        }
  });
}