#!/usr/bin/python
# -*- coding: utf-8 -*-

from couchbase.bucket import Bucket
from app.extensions.db_ext import calculate_hash
from json import dumps
import couchbase


class NoSQL(object):
    def __init__(self, app, bucket_name):
        self.cb = Bucket(
            "couchbase://{0}:{1}/{2}".format(
                app.config["COUCHBASE_ADDRESS"],
                app.config["COUCHBASE_PORT"],
                bucket_name),
            username=app.config["COUCHBASE_USERNAME"],
            password=app.config["COUCHBASE_PASSWORD"],
            lockmode=couchbase.LOCKMODE_WAIT
        )

    def write_key(self, arg, prefix, ttl):
        key = calculate_hash(arg.keys()[0])
        values = dumps(arg.values()[0])
        try:
            self.cb.insert(prefix + "-" + key, values, ttl=int(ttl))
        except couchbase.exceptions.KeyExistsError:
            self.cb.replace(prefix + "-" + key, values, ttl=int(ttl))
        return True

    def read_key(self, key, prefix):
        try:
            return self.cb.get(prefix + "-" + calculate_hash(key)).value
        except couchbase.exceptions.NotFoundError:
            return

    def delete_key(self, key, prefix):
        try:
            self.cb.delete(prefix + "-" + calculate_hash(key), quiet=True)
        except couchbase.exceptions.NotFoundError:
            pass
        finally:
            return True
