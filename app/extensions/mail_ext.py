#!/usr/bin/python
# -*- coding: utf-8 -*-

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


class Mail(object):
    def __init__(self, app):
        self.email = app.config["MAIL_ADDRESS"]
        self.password = app.config["MAIL_PASSWORD"]
        self.server = app.config["MAIL_SERVER"]
        self.port = 25
        self.session = None
        self.mail_from = app.config["MAIL_FROM"]
        if app.config["MAIL_SECURE"]:
            self.port = 587

    def start_session(self):
        session = smtplib.SMTP(self.server, self.port)
        session.ehlo()
        if self.port == 587:
            session.starttls()
            session.ehlo()
            session.login(self.email, self.password)
        self.session = session

    def send_mail(self, subject, body, recipients, html=False, attach=None):
        self.start_session()
        recipients_addr = []
        for k, v in recipients.items():
            if k in ["to", "cc"]:
                recipients_addr.extend(v)
        if not html:
            headers = [
                "From: " + self.mail_from,
                "Subject: " + subject,
                "To: " + ";".join(recipients["to"])]
            if "cc" in recipients:
                headers.append(
                    "Cc: " + ";".join(recipients["cc"])
                )
            headers = "\r\n".join(headers)
            self.session.sendmail(
                self.email,
                recipients_addr,
                headers + "\r\n\r\n" + body
            )
        else:
            msg = MIMEMultipart('alternative')
            msg['Subject'] = subject
            msg['From'] = self.mail_from
            msg['To'] = ";".join(recipients["to"])
            if "cc" in recipients:
                msg["Cc"] = ";".join(recipients["cc"])
            part2 = MIMEText(body, "html", "utf-8")
            msg.attach(part2)
            if attach is not None:
                filename = attach
                attachment = open(filename, "rb")
                part = MIMEBase('application', 'octet-stream')
                part.set_payload(attachment.read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', "attachment; filename= %s" % filename.split("/")[-1])
                msg.attach(part)
            self.session.sendmail(
                self.email,
                recipients_addr,
                msg.as_string()
            )
        return True
