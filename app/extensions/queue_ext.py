#!/usr/bin/python
# -*- coding: utf-8 -*-

from rq import Worker, Queue, Connection
from app import queue_manager, queue_name, q
from time import sleep


def queue_to_job(func_name, args, job_timeout=1200):
    q.enqueue_call(
        func=func_name, args=args, result_ttl=10, timeout=job_timeout
    )
    sleep(0.5)


def worker_manager():
    with Connection(queue_manager):
        worker = Worker(list(map(Queue, queue_name)))
        worker.work()
