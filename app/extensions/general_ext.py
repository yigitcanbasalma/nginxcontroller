#!/usr/bin/python
# -*- coding: utf-8 -*-

from crossplane import build, parse
from tempfile import NamedTemporaryFile
from app import db
from app.modules.admin.models import HostGroups
from app.modules.auth.models import UserRoles
from app.modules.main.models import SystemEvents


def get_host_group_alias(host_group_id):
    return HostGroups.query.with_entities(
        HostGroups.alias
    ).filter(
        HostGroups.id == host_group_id
    ).first().alias


def get_host_group_id(host_group_alias):
    return HostGroups.query.with_entities(
        HostGroups.id
    ).filter(
        HostGroups.alias == host_group_alias
    ).first().id


def get_user_role_name(user_role_id):
    return UserRoles.query.with_entities(
        UserRoles.role_name
    ).filter(
        UserRoles.id == user_role_id
    ).first().role_name


def sqlalchemy_as_dict(result_proxy, multiple=False):
    if multiple:
        result_list = list()
        for result in result_proxy:
            result_list.append(
                dict((col, getattr(result, col)) for col in result_proxy.__table__.columns.keys())
            )
        return result_list
    return dict((col, getattr(result_proxy, col)) for col in result_proxy.__table__.columns.keys())


def new_system_event(event, event_type="warning", auto_commit=False):
    new_event = SystemEvents(
        event_type=event_type,
        event=event
    )
    db.session.add(new_event)
    if auto_commit:
        db.session.commit()


def nginx_config_parser(raw_config):
    temp_config_file = NamedTemporaryFile()
    with open(temp_config_file.name, "w") as config_file:
        config_file.write(raw_config)
    # Test nginx config file
    return parse(temp_config_file.name, check_ctx=False, comments=True)
