#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Response, request
from json import dumps, loads
from datetime import datetime, date
from zlib import decompress


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def response_create(data, rtype="json", headers=None, response_code=200):
    avail_rsp = {
        "json": "application/json"
    }
    return Response(
        dumps(data, default=json_serial),
        mimetype=avail_rsp[rtype],
        headers=headers,
        status=response_code
    )


def compressed_response_extract(response):
    if request.headers.get("Accept-Encoding") in ["gzip, deflate"]:
        return loads(decompress(response))
    return list()
