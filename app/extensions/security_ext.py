#!/usr/bin/python
# -*- coding: utf-8 -*-


import re
import string
from random import choice, randint
from app.extensions.db_ext import calculate_hash, control_hash
from app.modules.auth.models import UserRoles


def escape_chars(target):
    return target.encode("utf-8")


def strong_password():
    characters = string.ascii_letters + string.digits + "_*!."
    return "".join(choice(characters) for _ in range(randint(8, 16)))


class StringArgument(object):
    def __init__(self, **kwargs):
        self.target = escape_chars(kwargs["target"])
        self.type = kwargs["data_type"]
        self.reg = ""
        self.min_length = kwargs["min_length"] if "min_length" in kwargs else 5
        self.max_length = kwargs["max_length"] if "max_length" in kwargs else 10
        self.special_chars = kwargs["special_chars"] if "special_chars" in kwargs else False
        self.numbers = kwargs["numbers"] if "numbers" in kwargs else False
        self.upper_case = kwargs["upper_case"] if "upper_case" in kwargs else False
        self.lower_case = kwargs["lower_case"] if "lower_case" in kwargs else False
        self.none_possible = kwargs["none_possible"] if "none_possible" in kwargs else False

    def play(self):
        if self.none_possible:
            return self.target
        if not self.special_chars and not self.numbers and not self.lower_case and not self.upper_case:
            self.reg += "[a-zşiğüöç ]{%s,%s}" % (self.min_length, self.max_length)
        else:
            tmp_pattern = "["
            if self.lower_case:
                tmp_pattern += "a-zşiğüöç"
            if self.upper_case:
                tmp_pattern += "A-ZŞİĞÜÖÇ"
            if self.numbers:
                tmp_pattern += "0-9"
            if self.special_chars:
                tmp_pattern += "_\-\."
            tmp_pattern += " ]{%s,%s}" % (self.min_length, self.max_length)
            self.reg += tmp_pattern
        if bool(re.search(self.reg, self.target)):
            return self.target.decode("utf8")
        raise ValueError("%s pattern is invalid!!!" % self.type)


class EmailArgument(object):
    def __init__(self, **kwargs):
        self.target = escape_chars(kwargs["target"])
        self.reg = "^[a-zA-Z0-9.\-_]+@[a-zA-Z0-9]{,8}\.([a-zA-Z0-9]{,8}\.[a-zA-Z0-9]{,8}|[a-zA-Z0-9]{,8})$"

    def play(self):
        if bool(re.search(self.reg, self.target)):
            return self.target
        raise ValueError("Email pattern is invalid!!!")


class PasswordArgument(object):
    def __init__(self, **kwargs):
        self.password = escape_chars(kwargs["password"])
        self.re_password = escape_chars(kwargs["re_password"])
        self.min_length = kwargs["min_length"] if "min_length" in kwargs else 8
        self.max_length = kwargs["max_length"] if "max_length" in kwargs else 16
        self.reg = "^(?=.*?\d)(?=.*?[A-Z])(?=.*?[@.*\-_!])(?=.*?[a-z])[A-Za-z\d@.*\-_!]{%s,%s}$" % (self.min_length, self.max_length)

    def play(self):
        if bool(re.search(self.reg, self.password)):
            hashed_password = calculate_hash(target=self.password)
            if control_hash(hashed_password, self.re_password):
                return hashed_password
            raise ValueError("Your passwords does not match.")
        raise ValueError("Your password must contains one digit (0-9), one special char (@.-_), one upper char and one lower char and at least 8 character.")


class BoolStringArgument(object):
    def __init__(self, **kwargs):
        self.target = escape_chars(kwargs["target"]) if isinstance(kwargs["target"], (str, unicode)) else kwargs["target"]
        self.none_possible = kwargs["none_possible"] if "none_possible" in kwargs else False
        self.data_type = kwargs["data_type"]

    def play(self):
        if self.none_possible:
            return "None"
        if self.target in ["False", "True", "true", "false", "Enabled", "enabled", "Disabled", "disabled", "Active", "Deactive", 1, 0]:
            return self.target
        raise ValueError("This '" + self.data_type + "' parameter is not bool available !!!")


class PhoneNumberArgument(object):
    def __init__(self, **kwargs):
        self.target = escape_chars(kwargs["target"])
        self.reg = "^\+[0-9]{12,20}"

    def play(self):
        if bool(re.search(self.reg, self.target)):
            return self.target
        raise ValueError("Your phone number is invalid. You should start plus (+) sign.")


class IPArgument(object):
    def __init__(self, **kwargs):
        self.target = escape_chars(kwargs["target"]).split(",") if isinstance(kwargs["target"], (str, unicode)) else kwargs["target"]
        self.target = set(self.target)
        self.reg = "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
        self.blocked_ip_pattern = [
            "127.0.0.1",
            "0.0.0.0"
        ]

    def play(self):
        for _ip in self.target:
            if not bool(re.search(self.reg, _ip)) or _ip in self.blocked_ip_pattern:
                raise ValueError("Invalid IP address found. IP: '{0}'".format(_ip))
        return ",".join(self.target)


class UserRoleArgument(object):
    def __init__(self, **kwargs):
        self.target = escape_chars(kwargs["target"])

    def play(self):
        has_exist = UserRoles.query.with_entities(
            UserRoles.id
        ).filter(
            UserRoles.id == self.target
        ).first()
        if not has_exist:
            raise ValueError("User role not found.")
        return int(self.target)
