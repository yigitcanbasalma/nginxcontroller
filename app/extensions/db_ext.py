#!/usr/bin/python
# -*- coding: utf-8 -*-

import uuid
import hashlib
import datetime


def get_uuid():
    return str(uuid.uuid4()).lower().split("-")[-1]


def get_token():
    return str(uuid.uuid4())


def calculate_hash(target, method="sha256"):
    method_dict = {
        "md5": hashlib.md5,
        "sha256": hashlib.sha256,
        "sha512": hashlib.sha512
    }
    return method_dict[method](target.encode("utf-8")).hexdigest()


def control_hash(*args, **kwargs):
    method = "sha256" if "method" not in kwargs else kwargs["method"]
    return args[0] == calculate_hash(args[1], method=method)


def datetime_pattern(pt=None, dt=None, ct=None):
    pattern_dict = {
        "ts": "%d-%m-%Y %H:%M:%S",
        "br": "%d/%m/%Y",
        "mysql": "%Y-%m-%d %H:%M:%S",
        "js": "%Y-%m-%d %H:%M",
        "copyrights": "%Y"
    }
    if pt is None:
        pt = "ts"
    if dt is not None:
        if ct is None:
            return dt.strftime(pattern_dict[pt])
        return datetime.datetime.strptime(dt, ct).strftime(pattern_dict[pt])
    return datetime.datetime.now().strftime(pattern_dict[pt])


def datetime_from_timestamp(ts_string):
    return datetime.datetime.fromtimestamp(ts_string)


def get_unix_timestamp():
    return int((datetime.datetime.now() - datetime.datetime(1970, 1, 1, 0, 0, 0)).total_seconds())
