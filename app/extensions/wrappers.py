#!/usr/bin/python
# -*- coding: utf-8 -*-

from functools import wraps
from flask import session, redirect, url_for, request
from sqlalchemy import and_, or_
from app import logger
from app.extensions.response_ext import response_create
from app.extensions.db_ext import calculate_hash
from app.modules.auth.controllers import force_quit
from app.modules.auth.models import UserRoles, Users
from app.modules.admin.models import AgentAPIKeys


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not session.get("logged-in"):
            return response_create(
                data={},
                headers={"Location": "/"},
                response_code=302
            )
        return f(*args, **kwargs)

    return decorated_function


def before_request_processing(restricted=False, api_restricted=False):
    def decorated_function(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            user_obj = Users.query.with_entities(
                Users.id
            ).filter(
                and_(
                    Users.id == session.get("user_id"),
                    or_(
                        Users.status == "Deactive",
                        Users.deleted == 1,
                        Users.role != session.get("role_id")
                    )
                )
            ).first()

            if session.get("logged-in") and request.path == "/":
                return redirect(url_for("main_router.dashboard_page"))

            if user_obj:
                return force_quit()

            # Restrictions for users
            if restricted:
                has_access = UserRoles.query.with_entities(
                    UserRoles.id
                ).filter(
                    and_(
                        UserRoles.role_name == session.get("role"),
                        or_(
                            UserRoles.permissions.like("%{0}%".format(f.__name__)),
                            # TODO: delete "all" keyword from permission
                            UserRoles.permissions.like("%all%")
                        )
                    )
                ).first()
                if not has_access:
                    logger.write_log(
                        event_type="unauthorized-access-attempt",
                        event_severity="ERROR",
                        event_username="system",
                        event_ip=request.headers.get("X-Forwarded-For"),
                        event="Unauthorized user role. URL Path: '{0}', Username: '{1}'".format(request.path, session.get("username"))
                    )
                    return response_create(
                        data={"status": "error", "message": "Forbidden zone."},
                        headers={"Location": url_for("main_router.dashboard_page")},
                        response_code=403
                    )

            # Restrictions for API users
            if api_restricted:
                # TODO: Check User-Agent header and confirm validation
                api_key = request.path.split("/")[2]
                source_ip = request.headers.get("X-Forwarded-For")
                has_access = AgentAPIKeys.query.with_entities(
                    AgentAPIKeys.id
                ).filter(
                    and_(
                        AgentAPIKeys.key_hash == calculate_hash(api_key),
                        AgentAPIKeys.status == "Active",
                        AgentAPIKeys.deleted == 0,
                        AgentAPIKeys.ip_address_pool.like("%{0}%".format(source_ip))
                    )
                ).first()
                if not has_access:
                    logger.write_log(
                        event_type="unauthorized-access-attempt",
                        event_severity="ERROR",
                        event_username="system",
                        event_ip=source_ip,
                        event="Unauthorized Agent API key. URL Path: '{0}'.".format(request.path)
                    )
                    return response_create(
                        data={"status": "error", "message": "Forbidden zone."},
                        response_code=403
                    )

            return f(*args, **kwargs)

        return wrapper

    return decorated_function
