#!/usr/bin/python
# -*- coding: utf-8 -*-

from app.extensions.db_ext import datetime_pattern

import logging
import os


class Logger(object):
    def __init__(self, app):
        logger = logging.getLogger("NginxController")
        handler = logging.FileHandler(os.path.join(app.config["LOGGING_BASE"], "application.log"))
        formatter = logging.Formatter("%(custom_timestamp)s %(event_type)s %(event_ip)s %(levelname)s %(event_user)s %(message)s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        logger.setLevel(logging.getLevelName(app.config["LOG_LEVEL"]))
        self.logging_convert_table = {
            "INFO": logger.info,
            "ERROR": logger.error,
            "WARNING": logger.warning,
            "CRITICAL": logger.critical
        }

    def write_log(self, event_type, event_ip, event_severity, event, event_username):
        logger_extras = {
            "custom_timestamp": datetime_pattern(),
            "event_type": event_type,
            "event_ip": event_ip,
            "event_user": event_username
        }
        self.logging_convert_table[event_severity](event, extra=logger_extras)
