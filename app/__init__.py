#!/usr/bin/python
# -*- coding: utf-8 -*-


# Import flask and template operators
from flask import Flask, session, request
from flask_sqlalchemy import SQLAlchemy
from flask_recaptcha import ReCaptcha
from flask_migrate import Migrate
from rq import Queue
from app.extensions.response_ext import response_create
from app.extensions.nosql_ext import NoSQL
from app.extensions.db_ext import datetime_pattern
from app.extensions.mail_ext import Mail
from app.extensions.filelogger_ext import Logger

import redis
import os

# Define the WSGI application object
app = Flask(__name__)

# Set project base
project_base = os.path.abspath(os.path.dirname(__file__))

# Configurations
app.config.from_object("app.config.dev") if not os.path.exists(os.path.join(os.getcwd(), "app/config/config.py")) else app.config.from_object("app.config.config")

# SQLAlchemy init
db = SQLAlchemy(app)

# Migration controller init
migrate = Migrate(app, db)

# Redis init
queue_manager = redis.from_url(app.config["RQ_REDIS_URL"])
queue_name = app.config["RQ_QUEUES"]
q = Queue(queue_name[0], connection=queue_manager)
scheduler_q = Queue(queue_name[1], connection=queue_manager)


# Create Database base model
class Base(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)


# Re-captcha init
re_captcha = ReCaptcha(app)

# Session manager init
if app.config["SESSION_MANAGER"] == "couchbase":
    from app.components.couchbase_session_interface import CouchbaseSessionInterface

    app.session_interface = CouchbaseSessionInterface(app)

# Couchbase buckets init
opened_buckets = dict()
for bucket in app.config["COUCHBASE_BUCKETS"]:
    opened_buckets[bucket] = NoSQL(app, bucket_name=bucket)

# Mail init
mail_manager = Mail(app)

# File logger init
logger = Logger(app)

# Set base folders
user_base = project_base + app.config["USER_BASE"]
mail_template_base = project_base + app.config["MAIL_TEMPLATE_BASE"]
temp_dir = app.config["TEMP_DIR"]
base_url = app.config["BASE_URL"]

# Set application config
permitted_file_types = app.config["PERMITTED_FILE_TYPES"]
nginx_base_location = app.config["NGINX_BASE_LOCATION"]
nginx_config_location = app.config["NGINX_CONFIG_LOCATION"]
remote_agent_configurations = app.config["REMOTE_AGENT_CONFIGURATION"]

# Start batch jobs
from app.scheduled_jobs.scheduler import schedule_all

schedule_all(
    rq_queue=scheduler_q,
    connection=queue_manager
)


# Session lifetime config
@app.before_request
def make_session_permanent():
    if session.get("logged-in") is not None:
        if request.path not in (
                "/main/system-events/list"
        ):
            session.permanent = True
            app.permanent_session_lifetime = app.config["PERMANENT_SESSION_LIFETIME"]


# Constraint values
@app.context_processor
def constraint():
    return dict(
        project_logo="/static/images/logo/logo.png",
        page_title="Nginx Open Source Controller",
        footer_text="Watcher {0}".format(datetime_pattern(pt="copyrights")),
        recaptcha=re_captcha
    )


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return response_create(
        data={
            "status": "error",
            "message": str(error).split(":")[1]
        },
        response_code=404
    )


@app.errorhandler(400)
def bad_request(error):
    return response_create(
        data={
            "status": "error",
            "message": str(error).split(":")[1]
        },
        response_code=400
    )


@app.errorhandler(401)
def auth_error(error):
    return response_create(
        data={
            "status": "error",
            "message": str(error).split(":")[1]
        },
        response_code=401
    )


@app.errorhandler(403)
def forbidden_error(error):
    return response_create(
        data={
            "status": "error",
            "message": str(error).split(":")[1]
        },
        response_code=403
    )


@app.errorhandler(405)
def method_error(error):
    return response_create(
        data={
            "status": "error",
            "message": str(error).split(":")[1]
        },
        response_code=405
    )


@app.errorhandler(500)
def internal_server_error(error):
    return response_create(
        data={
            "status": "error",
            "message": str(error).split(":")[1]
        },
        response_code=500
    )


if app:
    # Regular pages
    from app.modules.routes import router
    from app.modules.auth.routes import auth_router
    from app.modules.main.routes import main_router
    from app.modules.clusters.routes import clusters_router

    # Admin page
    from app.modules.admin.routes import admin_router

    # API Page
    from app.modules.agent.routes import agent_router

    # Register pages
    # Regular pages
    app.register_blueprint(router)
    app.register_blueprint(auth_router)
    app.register_blueprint(main_router)
    app.register_blueprint(clusters_router)
    # Admin page
    app.register_blueprint(admin_router)
    # API Page
    app.register_blueprint(agent_router)

# Build the database:
# This will create the database file using SQLAlchemy
db.create_all()
