#!/usr/bin/env bash

# Check UID
if [[ ${UID} != 0 ]]
then
    echo "This script has to run with root credentials."
    exit 1
fi

if [[ ! -f "$(pwd)/migrations" ]]
then
    echo "This script has to run in 'nginxcontroller' application base folder."
fi

# Get latest stable release
wget -O /tmp/latest.tar.gz https://bitbucket.org/yigitcanbasalma/nginxcontroller/get/latest.tar.gz

# Extract new codes folder to tmp
tar -xzf /tmp/latest.tar.gz -C /tmp

# Move new codes to production
cp -r /tmp/yigitcanbasalma-nginxcontroller-*/* .

# Cleanup tmp folder
rm -rf /tmp/latest.tar.gz
rm -rf /tmp/yigitcanbasalma-nginxcontroller-*

# Re-run requirements.txt
pip install -r requirements.txt

# Migration folder init
/usr/bin/flask db init

# Build SQL changes
/usr/bin/flask db migrate

# Run SQL changes
/usr/bin/flask db upgrade

# Restart all services
systemctl restart watcher && \
    systemctl restart watcher-async-jobs && \
    systemctl restart watcher-scheduler
