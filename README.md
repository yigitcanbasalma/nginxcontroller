### Yapısı ve Çalışma Prensibi
Uygulamanın çıkış amacı; n tane nginx uygulamasını merkezi olarak konfigüre edebilmek ve bu şekilde yönetebilmek. 

Uygulamanın ana bileşenlerinden biri, yine nginx firmasına ait olan "amplify-agent" ürününün modifiye edilmiş versiyonudur. İlgili uygulamaya [buradan](https://bitbucket.org/yigitcanbasalma/nginx-amplify-agent/src/master/) ulaşabilirsiniz. Çalışma prensibi olarak, ajanlar üzerinden gönderilen belirli intervaldeki dataları, kendi üzerinde yer alan kaynak datalar ile karşılaştırarak belirli çıktılar üretir. Uygulama, ajanüzerinden yollanan ilk datayı orijin olarak kabul eder ve bu dataya göre işlemlerini konumlandırır. Sisteme dahil olan sunucular, birer host grup (cluster) altında toplanır ve buna göre konfigürasyon şeması oluşturulur. İlk kayıt sırasında bu group "Default" adında özel bir gruptur. Özelliği, bu host gruba dahil olan sunucular için, konfigürasyon dosya tutarlılığı takip edilmez. Başka bir deyişle, bu sunucular üzerindeki konfigürasyon dosyaları değişse dahi, sistemde yer alan orjinal konfigürasyonlar bu ajanlara iletilmez.

### Kurulum
Uygulamanın çalışabilmesi için aşağıdaki bağımlılıklar gerekmektedir.

* Mysql (Database)
* Couchbase (Session Controller & Cache)
* Redis (Queue)
* Mail Gateway
* Google Re-Captcha (Optional)

Bu ayarlamaların ardından, ana dizinde yer alan "setup.sh" çalıştırılmalıdır. Geriye kalan adımlar için script sizi yönlendirecektir.

Ana konfigürasyon dosyamız, app/config/ dizininde yer alan config.py dosyasıdır. Bu alan içerisinde gerekli açıklamalar mevcuttur. Herhangi bir konfigürasyon düzenlemesi gerekmeyecektir. Ancak gerekirse, buradan yapabilirsiniz. __Kurulum sonrasında DNS kaydı yapmayı unutmayın.__

### Kullanım
Kurulumun ardından giriş yapmak için [http://watcher.local/](http://watcher.local/) adresine gidin. Kullanıcı "root", şifre "Root123*" şeklinde giriş yapabilirsiniz. İlk giriş sonrası şifrenizi değiştirmeyi unutmayın. Bunun için, sağ üst alandan "Profile" linkini kullanabilirsiniz. 

Uygulama üzerinde iki adet kullanıcı modu mevcut. Bunlardan birincisi "Admin" modu. Bu modda kullanıcı, sistem içerisinde istediği herşeyi yapabilir. Bu nedenle bu modu kullanırken veya bir başka kişiye verirken iyi düşünmek gereklidir. Diğer bir mod ise "User" modudur. Bu modda kullanıcı, sadece okuma ve listeleme işlemlerini yapabilir. Bunun dışında herhangi bir yetkisi bulunmaz.

Sunucu üzerinde bulunan ajanlar sisteme bağlanırken token kullanmak zorundadır. Ayrıca, token ile birlikte, sisteme bağlanacak olan ajanların IP adresleri de sisteme kayıt edilmelidir. Aksi taktirde bağlantı mümkün olmayacaktır. Bunun için, "Settings > System" menüsü altından "Agent API Key" oluşturulmalıdır. Oluşturulan API Key için tanımlanan IP adresleri daha sonra yeniden düzenlenebilir. __Fakat oluşturulan key, sadece tek sefer görünür olacaktır.__

Kullanıcı oluşturmak için "Settings > Users" tabı kullanılmalıdır. Burada oluşturulan kullanıcı için otomatik olarak şifre oluşacak ve kullanıcının mail adresine e-posta olarak gönderilecektir.

Sistem üzerinde oluşan kritik olaylar (örn. Nginx sunucuları üzerinde yapılan manuel konfigürasyon değişiklikleri) "System Events" sayfasında görüntülenir. Ayrıca kritik olay gerçekleşmeye başladığı anda, bu tab üzerinde bir uyarı işareti oluşur. Event üzerine tıklayarak bu eventi okundu olarak işaretleyebilirsiniz. Daha sonra, dilerseniz bu eventleri PDF yada Excel olarak export alabilirsiniz.

Sisteme kayıt olan sunucular "Clusters" tabından görüntülenir. Bu alanda hostlar, ait oldukları host grubuna (cluster) göre kümelenir. Hostlara ait nginx konfigürasyonları ise, host grubuna atanan kapsayıcı bir konfigürasyondur. Yani konfigürasyonlar hostlara değil, host gruplara aittir. Dahil olan sunucuların, bu konfigürasyonlar dışında bir konfigürasyona sahip olduğu saptanırsa, düzeltme için host grubun mevcut konfigürasyonu ajana iletilir ve sunucu konfigürasyonu düzeltilmiş olur. Kayıt olan sunuculara ilişkin özet ve detay bilgiler bu alanda gösterilir. Sunucu kartında bulunan "i" işaretine tıklayarak detay bilgileri görebilirsiniz. Ayrıca sistem üzerinde hostları aktif veya pasif duruma çekebilirsiniz. Bu durum, host üzerinde çalışan nginx uygulamasının durumunu değiştirmez. Yalnızca sistem üzerinde yer alan host kontrollerini devre dışı bırakır. Ayrıca host grubu da değiştirilebilir. 

Uygulama içinde oluşan tüm loglar "/var/log/ng_controller/application.log" dosyasından izlenebilir.

### Notlar
* Bu uygulama, gözlemlenmiş bir soruna çözüm olması için yazılmıştır. Eksikleri veya hataları olabilir. Bir geliştirici gözüyle bu uygulamayı değerlendirip, gerekli düzeltmeler konusunda benimle irtibata geçebilirsiniz. Amacım, açık kaynak dünyasına, yeni, faydalı ve işlevsel uygulamalar ortaya çıkartmaktır.

### Ekran Görüntüleri
![alt-text](https://i.ibb.co/T2d9g5Z/dashboard.png "Dashboard")
![alt-text](https://i.ibb.co/hLFvxkp/clusters.png "Clusters")
![alt-text](https://i.ibb.co/Cm9p8xk/host-info-detailed.png "Host Detailed Information")
![alt-text](https://i.ibb.co/j8SCpx8/host-info-detailed-2.png "Host Detailed Information 2")
![alt-text](https://i.ibb.co/s1jWcDB/host-config.png "Host Configuration")
![alt-text](https://i.ibb.co/fYVLYt2/cluster-nginx-configuration.png "Cluster Nginx Configuration")
![alt-text](https://i.ibb.co/Pxkr2NL/system-events.png "System Events")
![alt-text](https://i.ibb.co/4sTKMTk/settings.png "General Settings")
![alt-text](https://i.ibb.co/0cb9jZF/settings-users.png "User Settings")
![alt-text](https://i.ibb.co/SX68vZx/settings-pages.png "Page Settings")