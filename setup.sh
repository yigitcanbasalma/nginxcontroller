#!/usr/bin/env bash

# Check UID
if [[ ${UID} != 0 ]]
then
    echo "This script has to run with root credentials."
    exit 1
fi

# Mysql Configuration Section
read -p "Mysql host or IP address [localhost]: " mysql_host
MYSQL_HOST=${mysql_host:-localhost}
read -p "Mysql port [3306]: " mysql_port
MYSQL_PORT=${mysql_port:-3306}
read -p "Mysql username [root]: " mysql_username
MYSQL_USERNAME=${mysql_username:-root}
read -p "Mysql password: " -s mysql_password
MYSQL_PASSWORD=${mysql_password}
if [[ ${MYSQL_PASSWORD} == "" ]]
then
    echo "Mysql password can not be empty."
    exit 1
fi

## Connection String
MYSQL_CONN_STRING=mysql+pymysql://${MYSQL_USERNAME}:${MYSQL_PASSWORD}@${MYSQL_HOST}:${MYSQL_PORT}/ng_controller

echo
echo

# Couchbase Configuration Section
read -p "Couchbase host or IP address [localhost]: " couchbase_host
COUCHBASE_HOST=${couchbase_host:-localhost}
read -p "Couchbase port [8091]: " couchbase_port
COUCHBASE_PORT=${couchbase_port:-8091}
read -p "Couchbase username [Administrator]: " couchbase_username
COUCHBASE_USERNAME=${couchbase_username:-Administrator}
read -p "Couchbase password: " -s couchbase_password
COUCHBASE_PASSWORD=${couchbase_password}
if [[ ${COUCHBASE_PASSWORD} == "" ]]
then
    echo "Couchbase password can not be empty."
    exit 1
fi

echo
echo

# Redis Configuration Section
read -p "Redis host or IP address [localhost]: " redis_host
REDIS_HOST=${redis_host:-localhost}
read -p "Redis port [6379]: " redis_port
REDIS_PORT=${redis_port:-6379}

## Connection String
REDIS_CONN_STRING=redis://${REDIS_HOST}:${REDIS_PORT}/1

echo

# Mail Configuration Section
read -p "Mail server host or IP address: " mail_host
MAIL_HOST=${mail_host:-`exit 1`}
read -p "Mail address: " mail_address
MAIL_ADDRESS=${mail_address:-`exit 1`}
read -p "Mail password: " -s mail_password
MAIL_PASSWORD=${mail_password}
if [[ ${MAIL_PASSWORD} == "" ]]
then
    echo "Mail password can not be empty."
    exit 1
fi

echo
echo

# Google Re-Captcha Configuration Section
read -p "Google Re-Captcha site key [none]: " re_captcha_site_key
RE_CAPTCHA_SITE_KEY=${re_captcha_site_key:-none}
read -p "Google Re-Captcha secret key [none]: " re_captcha_secret_key
RE_CAPTCHA_SECRET_KEY=${re_captcha_secret_key:-none}

echo

# Root user mail
read -p "Root account e-mail address !IMPORTANT!" root_user_email
ROOT_USER_EMAIL=${root_user_email}
if [[ ${ROOT_USER_EMAIL} == "" ]]
then
    echo "Root user email can not be empty."
    exit 1
fi

echo

# Check OS
if [[ ! -f /etc/redhat-release ]]
then
    echo "This setup run for RPM based systems only."
    exit 1
else
    OS_VERSION=`python -c "from platform import platform; from re import search;  print search('centos-7', platform())"`
    if [[ ${OS_VERSION} == "None" ]]
    then
        echo "This setup run for OS version 7 or higher."
        exit 1
    fi
fi

# Check epel-release Repo
if [[ `rpm -qa | grep epel-release` == "" ]]
then
    yum install -y epel-release
fi

# Check Python Version
PYTHON_VERSION=`python -c "import platform; print ''.join(platform.python_version().split('.')[:2])"`
if [[ ${PYTHON_VERSION} != "27" ]]
then
    echo "This application has to be run with python 2.7.x version."
    exit 1
fi

# Install libcouchbase
## Import couchbase repo
rpm -ivh http://packages.couchbase.com/releases/couchbase-release/couchbase-release-1.0-6-x86_64.rpm
## Install necessaries packages
yum install -y libcouchbase-devel libcouchbase2-bin gcc gcc-c++ python-devel
## Remove couchbase repo
rpm -e $(rpm -qa | grep couchbase-release)

# Install pip and python dependencies
yum install -y python-pip && \
    pip install -r requirements.txt

# Modify config.py
mv app/config/config.py.example app/config/config.py
sed -i "s|MYSQL_CONN_STRING|${MYSQL_CONN_STRING}|g" app/config/config.py
sed -i "s/CB_HOST/${COUCHBASE_HOST}/g" app/config/config.py
sed -i "s/CB_PORT/${COUCHBASE_PORT}/g" app/config/config.py
sed -i "s/CB_USER/${COUCHBASE_USERNAME}/g" app/config/config.py
sed -i "s/CB_PASSWORD/${COUCHBASE_PASSWORD}/g" app/config/config.py
sed -i "s|REDIS_CONN_STRING|${REDIS_CONN_STRING}|g" app/config/config.py
sed -i "s/EMAIL_ADDRESS/${MAIL_ADDRESS}/g" app/config/config.py
sed -i "s/EMAIL_PASSWORD/${MAIL_PASSWORD}/g" app/config/config.py
sed -i "s/EMAIL_SERVER/${MAIL_HOST}/g" app/config/config.py
sed -i "s/GOOGLE_RECAPTCHA_SITE_KEY/${RE_CAPTCHA_SITE_KEY}/g" app/config/config.py
sed -i "s/GOOGLE_RECAPTCHA_SECRET_KEY/${RE_CAPTCHA_SECRET_KEY}/g" app/config/config.py

# Install and configure nginx as a reverse proxy
yum install -y nginx && \
    sed -i "s|BASE_LOCATION|`pwd`|g" setup/web/watcher.conf.nginx && \
    cp setup/web/watcher.conf.nginx /etc/nginx/conf.d/watcher.conf && \
    systemctl enable nginx && \
    systemctl start nginx

# Configure service files
sed -i "s|BASE_LOCATION|`pwd`|g" setup/services/watcher.service
sed -i "s|BASE_LOCATION|`pwd`|g" setup/services/watcher-async-jobs.service
sed -i "s|REDIS_CONN_STRING|${REDIS_CONN_STRING}|g" setup/services/watcher-async-jobs.service
chmod -x setup/services/*.service
cp setup/services/*.service /usr/lib/systemd/system/

# Import SQL files
sed -i "s|root\@local|${ROOT_USER_EMAIL}|g" setup/sql/base.sql
mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} < setup/sql/base.sql
mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} < setup/sql/base_functions.sql

# Create log folder
mkdir -p /var/log/ng_controller

# Start and enable applications
systemctl enable watcher && \
    systemctl start watcher
systemctl enable watcher-async-jobs && \
    systemctl start watcher-async-jobs
systemctl enable watcher-scheduler && \
    systemctl start watcher-scheduler